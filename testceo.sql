-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 16, 2018 at 04:18 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testceo`
--

-- --------------------------------------------------------

--
-- Table structure for table `bancos`
--

DROP TABLE IF EXISTS `bancos`;
CREATE TABLE IF NOT EXISTS `bancos` (
  `id_banco` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date NOT NULL,
  `Descripcion` varchar(199) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `Referencia` varchar(199) DEFAULT NULL,
  `Cargos` decimal(10,2) DEFAULT NULL,
  `Abonos` decimal(10,2) DEFAULT NULL,
  `Estatus_comprobacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `Banco` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `Pagina` varchar(25) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_banco`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cajachica`
--

DROP TABLE IF EXISTS `cajachica`;
CREATE TABLE IF NOT EXISTS `cajachica` (
  `id_caja` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date NOT NULL,
  `Concepto` varchar(150) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `Entrada` decimal(10,2) DEFAULT NULL,
  `Salida` decimal(10,2) DEFAULT NULL,
  `Saldo` decimal(10,2) DEFAULT NULL,
  `Pagina` varchar(25) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_caja`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cheques`
--

DROP TABLE IF EXISTS `cheques`;
CREATE TABLE IF NOT EXISTS `cheques` (
  `id_cheques` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date NOT NULL,
  `Banco` varchar(20) NOT NULL,
  `Cheque` int(8) NOT NULL,
  `Cliente` varchar(50) DEFAULT NULL,
  `Subtotal` decimal(10,2) DEFAULT NULL,
  `Total` decimal(10,2) DEFAULT NULL,
  `Estado` varchar(15) NOT NULL,
  PRIMARY KEY (`id_cheques`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `control_interno`
--

DROP TABLE IF EXISTS `control_interno`;
CREATE TABLE IF NOT EXISTS `control_interno` (
  `id_ci` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date NOT NULL,
  `Cantidad` decimal(10,2) NOT NULL,
  `Empresa` varchar(50) NOT NULL,
  `Contratista` varchar(50) NOT NULL,
  `Documento` varchar(50) NOT NULL,
  `Pagina` varchar(25) NOT NULL,
  PRIMARY KEY (`id_ci`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cpc`
--

DROP TABLE IF EXISTS `cpc`;
CREATE TABLE IF NOT EXISTS `cpc` (
  `id_cpc` int(11) NOT NULL AUTO_INCREMENT,
  `Concepto` varchar(100) DEFAULT NULL,
  `Empresa` varchar(100) DEFAULT NULL,
  `Cantidad` decimal(10,2) NOT NULL,
  `Fecha_limite` date NOT NULL,
  `Nota` varchar(200) DEFAULT NULL,
  `Estatus` varchar(25) NOT NULL,
  `Pagina` varchar(25) NOT NULL,
  PRIMARY KEY (`id_cpc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cpp`
--

DROP TABLE IF EXISTS `cpp`;
CREATE TABLE IF NOT EXISTS `cpp` (
  `id_cpp` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date NOT NULL,
  `Concepto` varchar(99) COLLATE latin1_spanish_ci NOT NULL,
  `Empresa` varchar(99) COLLATE latin1_spanish_ci NOT NULL,
  `Importe` decimal(10,2) NOT NULL,
  `Nota` varchar(199) COLLATE latin1_spanish_ci NOT NULL,
  `Estado` varchar(99) COLLATE latin1_spanish_ci NOT NULL,
  `Pagina` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_cpp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `facturas`
--

DROP TABLE IF EXISTS `facturas`;
CREATE TABLE IF NOT EXISTS `facturas` (
  `id_facturas` int(11) NOT NULL AUTO_INCREMENT,
  `Folio` varchar(37) COLLATE latin1_spanish_ci NOT NULL,
  `Concepto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `Fecha` date DEFAULT NULL,
  `Importe` decimal(10,2) DEFAULT NULL,
  `Retenciones` decimal(10,2) DEFAULT NULL,
  `IVA` decimal(10,2) DEFAULT NULL,
  `Moneda` varchar(9) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Total` decimal(10,2) DEFAULT NULL,
  `Razon_social` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Estado` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `Pagina` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  `Mes` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `Anio` int(5) NOT NULL,
  PRIMARY KEY (`id_facturas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fondofijo`
--

DROP TABLE IF EXISTS `fondofijo`;
CREATE TABLE IF NOT EXISTS `fondofijo` (
  `id_ff` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date NOT NULL,
  `Folio` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `Razon_social` varchar(99) COLLATE latin1_spanish_ci NOT NULL,
  `RFC` varchar(14) COLLATE latin1_spanish_ci NOT NULL,
  `Concepto` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Monto` decimal(10,2) NOT NULL,
  `IVA` decimal(10,2) NOT NULL,
  `Salida` decimal(10,2) NOT NULL,
  `Saldo` decimal(10,2) NOT NULL,
  `Banco` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `Pagina` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  `Mes` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `Anio` int(5) NOT NULL,
  PRIMARY KEY (`id_ff`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `importefondofijo`
--

DROP TABLE IF EXISTS `importefondofijo`;
CREATE TABLE IF NOT EXISTS `importefondofijo` (
  `id_iff` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date NOT NULL,
  `Importe` decimal(10,2) NOT NULL,
  `Banco` varchar(13) COLLATE latin1_spanish_ci NOT NULL,
  `Pagina` varchar(99) COLLATE latin1_spanish_ci NOT NULL,
  `Mes` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `Anio` int(5) NOT NULL,
  PRIMARY KEY (`id_iff`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nomina`
--

DROP TABLE IF EXISTS `nomina`;
CREATE TABLE IF NOT EXISTS `nomina` (
  `id_nomina` int(11) NOT NULL AUTO_INCREMENT,
  `Folio` varchar(13) COLLATE latin1_spanish_ci NOT NULL,
  `Periodo` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `Importe` decimal(10,2) DEFAULT NULL,
  `IMSS` decimal(10,2) DEFAULT NULL,
  `Subsidio` decimal(10,2) DEFAULT NULL,
  `ISR` decimal(10,2) DEFAULT NULL,
  `Empleado` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Pagina` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_nomina`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requerimientos`
--

DROP TABLE IF EXISTS `requerimientos`;
CREATE TABLE IF NOT EXISTS `requerimientos` (
  `id_req` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date NOT NULL,
  `Entidad` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `Documento` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `Solicitud` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `Folio` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `Comentarios` varchar(99) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Pagina` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_req`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sat_nuevo`
--

DROP TABLE IF EXISTS `sat_nuevo`;
CREATE TABLE IF NOT EXISTS `sat_nuevo` (
  `id_satn` int(11) NOT NULL AUTO_INCREMENT,
  `Mes` varchar(11) COLLATE latin1_spanish_ci NOT NULL,
  `Año` int(4) NOT NULL,
  `Ntotal` decimal(10,2) NOT NULL,
  `Nfpre` date NOT NULL,
  `Nfpag` date DEFAULT NULL,
  `C1total` decimal(10,2) DEFAULT NULL,
  `C1fpre` date DEFAULT NULL,
  `C1fpag` date DEFAULT NULL,
  `C2total` decimal(10,2) DEFAULT NULL,
  `C2fpre` date DEFAULT NULL,
  `C2fpag` date DEFAULT NULL,
  `C3total` decimal(10,2) DEFAULT NULL,
  `C3fpre` date DEFAULT NULL,
  `C3fpag` date DEFAULT NULL,
  `C4total` decimal(10,2) DEFAULT NULL,
  `C4fpre` date DEFAULT NULL,
  `C4fpag` date DEFAULT NULL,
  `Pagina` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_satn`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `sat_nuevo`
--

INSERT INTO `sat_nuevo` (`id_satn`, `Mes`, `Año`, `Ntotal`, `Nfpre`, `Nfpag`, `C1total`, `C1fpre`, `C1fpag`, `C2total`, `C2fpre`, `C2fpag`, `C3total`, `C3fpre`, `C3fpag`, `C4total`, `C4fpre`, `C4fpag`, `Pagina`) VALUES
(1, 'Enero', 2018, '10000.00', '2018-01-01', '2018-01-01', '0.00', '0000-00-00', '0000-00-00', '0.00', '0000-00-00', '0000-00-00', '0.00', '0000-00-00', '0000-00-00', '0.00', '0000-00-00', '0000-00-00', 'CEO');

-- --------------------------------------------------------

--
-- Table structure for table `servicios`
--

DROP TABLE IF EXISTS `servicios`;
CREATE TABLE IF NOT EXISTS `servicios` (
  `id_servicios` int(11) NOT NULL AUTO_INCREMENT,
  `Tipo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `Servicio` varchar(99) COLLATE latin1_spanish_ci NOT NULL,
  `Folio` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `Periodo` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Fecha_emision` date NOT NULL,
  `Fecha_pago` date DEFAULT NULL,
  `Forma_pago` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Importe` decimal(10,2) DEFAULT NULL,
  `Pagina` varchar(99) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_servicios`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `servicios`
--

INSERT INTO `servicios` (`id_servicios`, `Tipo`, `Servicio`, `Folio`, `Periodo`, `Fecha_emision`, `Fecha_pago`, `Forma_pago`, `Importe`, `Pagina`) VALUES
(1, 'Telefonia e Internet', 'TOTALPLAY', '34543', 'Mensual', '2018-07-03', '2018-07-26', 'Efectivo', '542.99', 'CEO'),
(2, 'Luz', 'AXTEL', '65645', 'Mensual', '2018-07-09', '2018-07-31', 'Efectivo', '6546.05', 'CEO');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `login` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `nivel` int(1) DEFAULT NULL,
  PRIMARY KEY (`login`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`login`, `password`, `nombre`, `nivel`) VALUES
('admin', 'admin', 'admin', 1),
('Mauricio', '54321', 'Mauricio', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

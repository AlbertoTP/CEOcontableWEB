<footer class="sticky-bottom pb-3 pl-5 pl-5">
  <div class="container pt-5 pb-5">
    <div class="row justify-content-center align-items-center">
      <!-- <div class="col-1">
      </div> -->
      <div class="col-12 col-sm">
        <h5 class="titlefooter">Contacto</h5>
        <ul class="list-unstyled">
          <p class="textfooter">
          	<li><i class="fa fa-map-marker"></i> Calzada Zavaleta 1306-A Local 13 Col. Santa Cruz Buenavista, CP.72170 Puebla, Pue. México.</li>
          	<li><i class="fa fa-phone"></i> +52 (222) 4.04.29.00 con 8 Líneas</li>
          	<li><i class="fa fa-phone"></i> 01.800.666.99.99</li>
          	<li><i class="fa fa-envelope"></i> info@ceoconsulting.com.mx</li>
          </p>
        </ul>
      </div>
      <div class="col-12 col-sm align-self-center text-center">
        <a href="http://ceoconsulting.com.mx/" title="CEO Consulting ">
          <img class="" src="<?php echo base_url();?>static\icons\mstile-150x150.png" alt="" width="150px" height="auto">
        </a>
      </div>
    </div>
  </div>
  <section>
		<div class="container">
			<div class="row">
				<div class="col-12">
          <div>
            <small>Copyright © 2018 CEO Consulting . Todos los derechos reservados.
            </small>
            <p class="text-right">
              Desarrollado por: <br />
              <a href="https://albertotp.github.io/" target="_blank" class="text-light">Alberto Tapia</a>
              <br />
              <a href="https://crinesx.github.io/" target="_blank" class="text-light">Mauricio Rodriguez</a>
            </p>
          </div>
				</div>
			</div>
		</div>
	</section>

</footer>

</html>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<script>window.jQuery || document.write('<script src="<?php echo base_url();?>static/bootstrap/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="<?php echo base_url();?>static\bootstrap\assets\js\vendor\popper.min.js"></script>
<script src="<?php echo base_url();?>static\bootstrap\js\bootstrap.min.js"></script>
<!-- <script>
$( '#navbarSupportedContent .navbar-nav li' ).on( 'click', function () {
  $( '#navbarSupportedContent .navbar-nav' ).find( 'li.active' ).removeClass( 'active' );
  $( this ).parent( 'li' ).addClass( 'active' );
});
</script> -->

<!-- waves -->
<script type="text/javascript" src="<?php echo base_url();?>static\waves\waves.min.js"></script>
<script type="text/javascript">
    // This is ok.
    Waves.init();
    Waves.attach('.btn', ['waves-float', 'waves-float']);
</script>

<?php if($this->session->userdata('nivel')==('1'||'2'))  {?>

<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light navbar-custom">
  <a class="navbar-brand text-center" href="<?php echo base_url();?>index.php/Welcome/bienvenida">
    <img class="animated pulse" src="<?php echo base_url();?>static\icons\logo.png" width="60%" height="auto" alt="">
    <!-- <?php echo $this->session->userdata('nombre'); ?> -->
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">

      <?php if ($this->session->userdata('nivel')=='1'){
        echo '<li class="nav-item"><a class="nav-link" href="';
        echo base_url();
        echo "index.php/Welcome/usuarios";
        echo ' ">USUARIOS</a></li> ';
       } ?>
      <!-- <li class="nav-item active"><a class="nav-link" href="<?php echo base_url();?>index.php/Welcome/bienvenida">Home</a></li> -->
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>index.php/Welcome/caja">CAJA CHICA</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>index.php/Welcome/banco">BANCO</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>index.php/Welcome/facturas">FACTURAS</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>index.php/Welcome/nomina">NÓMINA</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>index.php/Welcome/servicios">SERVICIOS</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>index.php/Welcome/sat">SAT</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>index.php/Welcome/requerimientos">REQUERIMIENTOS</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>index.php/Welcome/controlIntterno">CONTROL INTERNO</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>index.php/Welcome/cheques">CHEQUES</a></li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          FONDO FIJO
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url();?>index.php/Welcome/fondoFijo">FONDO FIJO</a>
          <a class="dropdown-item" href="<?php echo base_url();?>index.php/Welcome/impfondoFijo">IMPORTE FONDO FIJO</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          CUENTAS
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url();?>index.php/Welcome/cpp">CPP</a>
          <a class="dropdown-item" href="<?php echo base_url();?>index.php/Welcome/cpc">CPC</a>
        </div>
      </li>

      <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>index.php/welcome/cerrarSesion">
        <i class="fas fa-sign-out-alt fa-1x"></i>
        <!-- Cerrar Sesión -->
      </a></li>

    </ul>
  </div>
</nav>
<?php } ?>

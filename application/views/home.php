<body>
<?php if(($this->session->userdata('nivel')>='1') &&   ($this->session->userdata('nivel')<='2')){?>

  <header class="pt-5" max-width="800px">
    <div id="carouselExampleIndicators" class="carousel slide pt-1" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      </ol>
      <div class="carousel-inner bgtexture" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <!-- <div class="carousel-item active" style="background-image: url('<?php echo base_url();?>static/img/fondoceo0.jpg')"> -->
        <div class="carousel-item active">
          <div class="row justify-content-md-center align-items-center">
            <div class="col-md-auto align-self-center">
              <p class="titlebig animated bounceInLeft bienvenido01">Bienvenido<br /> <?php echo $this->session->userdata('nombre'); ?></p>
            </div>
            <div class="col-md-auto pt-5 pb-5">
              <img class="img-fluid animated bounceInLeft arquero01" src="<?php echo base_url();?>static\img\arquero.png" alt="First slide">
            </div>
          </div>
        </div>
        <!-- Slide Two - Set the background image for this slide in the line below -->
        <div class="carousel-item">
          <div class="row justify-content-md-center align-items-center containerfull">
            <div class="col-md-auto align-self-center">
              <p class="titlebig animated bounceInDown app01">CEO Consulting Group</p><br />
              <p class="title text-center animated bounceInDown app02">Aplicación WEB de contabilidad</p>
            </div>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </header>

  <div class="container pt-5 pb-5">
    <div class="container pt-5">
      <h2>Descarga de formatos:</h2><br />
      <div class="row align-items-center">

        <div class="col-md align-self-center">
          <justify>
            <p>
              En esta sección se encuentran los archivos excel con el formato adecuado para la Aplicación WEB de contabilidad.
              A continuación encontraras los enlaces para descargar los diferentes formatos a tu dispositivo.<br /><br /><br />
              Se recomienda leer las instrucciones y recomendaciones que se encuentran debajo de esta sección antes de usar la aplicación.
            </p>
          </justify>
          <br />
          <br />
          <br />
          <center>
            <img class="img-fluid" src="<?php echo base_url();?>static\icons\android-chrome-192x192.png" alt="" >
          </center>
        </div>

        <div class="col align-self-center">

          <div class="list-group" style="font-size: 1.1rem; ">

            <a href="<?php echo base_url();?>formatosexcel\banco.xlsx" class="list-group-item list-group-item-action">
              <span class="figure-img img-fluid" style="color: MediumSeaGreen;">
                <i class="far fa-file-excel"></i>
              </span>
              Banco
            </a>

            <a href="<?php echo base_url();?>formatosexcel\cajachica.xlsx" class="list-group-item list-group-item-action">
              <span class="figure-img img-fluid" style="color: MediumSeaGreen;">
                <i class="far fa-file-excel"></i>
              </span>
              Caja chica
            </a>

            <a href="<?php echo base_url();?>formatosexcel\cheques.xlsx" class="list-group-item list-group-item-action">
              <span class="figure-img img-fluid" style="color: MediumSeaGreen;">
                <i class="far fa-file-excel"></i>
              </span>
              Cheques
            </a>

            <a href="<?php echo base_url();?>formatosexcel\controlinterno.xlsx" class="list-group-item list-group-item-action">
              <span class="figure-img img-fluid" style="color: MediumSeaGreen;">
                <i class="far fa-file-excel"></i>
              </span>
              Control Interno
            </a>

            <a href="<?php echo base_url();?>formatosexcel\cpc.xlsx" class="list-group-item list-group-item-action">
              <span class="figure-img img-fluid" style="color: MediumSeaGreen;">
                <i class="far fa-file-excel"></i>
              </span>
              CPC
            </a>

            <a href="<?php echo base_url();?>formatosexcel\cpp.xlsx" class="list-group-item list-group-item-action">
              <span class="figure-img img-fluid" style="color: MediumSeaGreen;">
                <i class="far fa-file-excel"></i>
              </span>
              CPP
            </a>

            <a href="<?php echo base_url();?>formatosexcel\factura.xlsx" class="list-group-item list-group-item-action">
              <span class="figure-img img-fluid" style="color: MediumSeaGreen;">
                <i class="far fa-file-excel"></i>
              </span>
              Facturas
            </a>

            <a href="<?php echo base_url();?>formatosexcel\fondofijo.xlsx" class="list-group-item list-group-item-action">
              <span class="figure-img img-fluid" style="color: MediumSeaGreen;">
                <i class="far fa-file-excel"></i>
              </span>
              Fondo fijo
            </a>

            <a href="<?php echo base_url();?>formatosexcel\nomina.xlsx" class="list-group-item list-group-item-action">
              <span class="figure-img img-fluid" style="color: MediumSeaGreen;">
                <i class="far fa-file-excel"></i>
              </span>
              Nómina
            </a>

            <a href="<?php echo base_url();?>formatosexcel\requerimiento.xlsx" class="list-group-item list-group-item-action">
              <span class="figure-img img-fluid" style="color: MediumSeaGreen;">
                <i class="far fa-file-excel"></i>
              </span>
              Requerimientos
            </a>

            <a href="<?php echo base_url();?>formatosexcel\sat.xlsx" class="list-group-item list-group-item-action">
              <span class="figure-img img-fluid" style="color: MediumSeaGreen;">
                <i class="far fa-file-excel"></i>
              </span>
              SAT
            </a>

            <a href="<?php echo base_url();?>formatosexcel\servicios.xlsx" class="list-group-item list-group-item-action">
              <span class="figure-img img-fluid" style="color: MediumSeaGreen;">
                <i class="far fa-file-excel"></i>
              </span>
              Servicios
            </a>

          </div>

        </div>

      </div>
    </div>
  </div>

  <div class="container pt-5 pb-5">
    <div class="container pt-5">
      <h2>Ver manual de uso</h2><br />
      <p>
        <a href="#" class="btn btn-primary disabled">Manual</a>
      </p>
      <center>
        <img class="img-fluid" src="<?php echo base_url();?>static/img/ciudad.png" alt="" >
      </center>
    </div>
  </div>

    <?php } else
   redirect('/Welcome/index/', 'refresh');
 ?>
</body>

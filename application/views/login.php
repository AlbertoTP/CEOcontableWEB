
<!-- login style -->
<link href="<?php echo base_url();?>static\bootstrap\css\signin.css" rel="stylesheet">
<!-- waves -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static\waves\waves.min.css" />

<body class="text-center bg">
    <form class="form-signin animated zoomIn" method="POST" action="<?php echo base_url();?>index.php/welcome/validaLogin">
      <img class="responsive" src="<?php echo base_url();?>static\icons\logo.png">
      <br />
      <br />
      <br />
      <!-- <h1 class="h3 mb-3 font-weight-normal">CEO contable</h1><br /> -->
      <h6>Ingresa tus datos</h6>

      <label for="inputEmail" class="sr-only">Login:</label>
      <input type="text" class="form-control" id="login" name="login" placeholder="Usuario" required>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Contraseña" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2018</p>
    </form>
  </body>

  <script type="text/javascript" src="<?php echo base_url();?>static\waves\waves.min.js"></script>
  <script type="text/javascript">
      // This is ok.
      Waves.init();
      Waves.attach('.btn', ['waves-float', 'waves-float']);
  </script>

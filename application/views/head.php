<!DOCTYPE html>
<html lang="es">
<head>
  <title>CEO contable</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url();?>static\bootstrap\css\bootstrap.min.css">
  <link href="<?php echo base_url();?>static/bootstrap/css/estilo.css" rel="stylesheet">
  <!-- animate -->
  <link href="<?php echo base_url();?>static/css/animate.css" rel="stylesheet">
  <!-- fontawesome -->
  <link href="<?php echo base_url();?>static/fontawesome/css/all.css" rel="stylesheet">
  <!-- carousel -->
  <link href="<?php echo base_url();?>static\bootstrap\css\full-slider.css" rel="stylesheet">
  <!-- waves -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static\waves\waves.min.css" />

  <!-- texthover -->
  <!-- <link href="<?php echo base_url();?>static\css\texthover.css" rel="stylesheet"> -->

  <!-- fuentes -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,600italic,400italic,300italic" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

  <!--
  <link rel="stylesheet" href="<?php echo base_url();?>/css/materialize.min.css">
  <script src="<?php echo base_url();?>/js/materialize.min.js"></script>
  -->

  <!-- chart.js -->
  <script src="<?php echo base_url();?>/js/Chart.bundle.min.js"></script>
  <script src="<?php echo base_url();?>/js/Chart.bundle.js"></script>
  <style>
    .bg {
        /* The image used */
        background-image: url("<?php echo base_url();?>static/img/fondoceo0.jpg");
        /* Full height */
        height: 100%;
        /* Center and scale the image nicely */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .bgtexture {
        background-image: url("<?php echo base_url();?>static/img/textura.jpg");
        background-repeat: repeat;
    }
    .bgtitle{
      background-image: url("<?php echo base_url();?>static/img/bg-masthead.jpg");
      background-repeat: no-repeat;
      background-size: cover;
    }
    @media screen and (max-width: 800px) {
      header {
        display: none;
      }
    }
  </style>

  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>static\icons\apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>static\icons\favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>static\icons\favicon-16x16.png">
  <link rel="manifest" href="<?php echo base_url(); ?>static\icons\site.webmanifest">
  <link rel="mask-icon" href="<?php echo base_url(); ?>static\icons\safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#000000">
  <meta name="theme-color" content="#ffffff">

</head>

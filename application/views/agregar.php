<body>
<?php if(($this->session->userdata('nivel')>='1') &&   ($this->session->userdata('nivel')<='2')){?>

<section class="maincontainer">
  <div class="container-fluid espacioforms">
    <?php
    switch ($datos) {//Acción
      case 'caja': ?>
        <h2>Agregar elemento Caja chica</h2><br />
        <form action="<?php echo base_url();?>index.php/welcome/agregar_caja" method="POST" enctype="multipart/form-data">

          <div class="form-group row">
            <label for="Fecha" class="col-md-2 col-form-label">Fecha:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="Fecha" id="Fecha" min="2000-01-01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Concepto" class="col-md-2 col-form-label">Concepto: </label>
            <div class="col-10">
              <textarea class="form-control" id="Concepto" name="Concepto" rows="3" cols="50" maxlength="150" placeholder="Concepto ..."></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="Entrada" class="col-md-2 col-form-label">Entrada: </label>
            <div class="col-10">
              <input class="form-control" type="number" name="Entrada" id="Entrada" step="0.01">
            </div>
          </div>

          <div class="form-group row">
            <label for="Salida" class="col-md-2 col-form-label">Salida: </label>
            <div class="col-10">
              <input class="form-control" type="number" name="Salida" id="Salida" step="0.01">
            </div>
          </div>

          <div class="form-group row">
            <label for="Saldo" class="col-md-2 col-form-label">Saldo: </label>
            <div class="col-10">
              <input class="form-control" type="number" name="Saldo" id="Saldo" step="0.01">
            </div>
          </div>

          <div class="form-group row">
            <label for="radio" class="col-md-2 col-form-label">Página: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio01" name="radio" value="CEO" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio01">CEO</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio02" name="radio" value="GENIOS" class="custom-control-input">
                   <label class="custom-control-label" for="radio02">GENIOS</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio03">otro</label>
                   <input type="text" placeholder="Página" name="pag" class="form-control form-control-sm" />
              </div>
            </div>
          </div>

        <?php
        break;
      //-----------------------------------------------
      case 'banco': ?>
        <h2>Agregar elemento Banco</h2>
        <form action="<?php echo base_url();?>index.php/welcome/agregar_banco" method="POST" enctype="multipart/form-data">

          <div class="form-group row">
            <label for="Fecha" class="col-md-2 col-form-label">Fecha:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="Fecha" id="Fecha" min="2000-01-01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Descripcion" class="col-md-2 col-form-label">Descripción: </label>
            <div class="col-10">
              <textarea class="form-control" id="Descripcion" name="Descripcion" rows="3" cols="50" maxlength="199" placeholder="Descripción ..."></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="Referencia" class="col-md-2 col-form-label">Referencia: </label>
            <div class="col-10">
              <textarea class="form-control" id="Referencia" name="Referencia" rows="3" cols="50" maxlength="199" placeholder="Referencia ..."></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="Cargo" class="col-md-2 col-form-label">Cargo: </label>
            <div class="col-10">
              <input class="form-control" type="number" name="Cargo" id="Cargo" step="0.01">
            </div>
          </div>

          <div class="form-group row">
            <label for="Abonos" class="col-md-2 col-form-label">Abonos: </label>
            <div class="col-10">
              <input class="form-control" type="number" name="Abonos" id="Abonos" step="0.01">
            </div>
          </div>

          <div class="form-group row">
            <label for="Estatus_comprobacion" class="col-md-2 col-form-label">Estatus comprobación: </label>
            <div class="col-10">
              <input class="form-control" type="text" name="Estatus_comprobacion" id="Estatus_comprobacion" maxlength="50">
            </div>
          </div>

          <div class="form-group row">
            <label for="Total" class="col-md-2 col-form-label">Banco: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio11" name="radio1" value="BBVA BANCOMER" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio11">BBVA BANCOMER</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio12" name="radio1" value="INBURSA" class="custom-control-input">
                   <label class="custom-control-label" for="radio12">INBURSA</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio13" name="radio1" value="SCOTIABANK" class="custom-control-input">
                   <label class="custom-control-label" for="radio13">SCOTIABANK</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio14" name="radio1" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio14">otro</label>
                   <input type="text" placeholder="Banco" name="banco" class="form-control form-control-sm" />
              </div>
            </div>
          </div>

          <div class="form-group row">
            <label for="radio" class="col-md-2 col-form-label">Página: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio01" name="radio" value="CEO" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio01">CEO</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio02" name="radio" value="GENIOS" class="custom-control-input">
                   <label class="custom-control-label" for="radio02">GENIOS</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio03">otro</label>
                   <input type="text" placeholder="Página" name="pag" class="form-control form-control-sm" />
              </div>
            </div>
          </div>
        <?php
        break;
      //-----------------------------------------------
      case 'fondo': ?>
        <h2>Agregar elemento Fondo fijo</h2>
        <form action="<?php echo base_url();?>index.php/welcome/agregar_fondofijo" method="POST" enctype="multipart/form-data">

          <div class="form-group row">
            <label for="Fecha" class="col-md-2 col-form-label">Fecha:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="Fecha" id="Fecha" min="2000-01-01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Folio" class="col-md-2 col-form-label">Folio: </label>
            <div class="col-10">
              <input class="form-control" type="number" name="Folio" id="Folio" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Razon_social" class="col-md-2 col-form-label">Razón social: </label>
            <div class="col-10">
              <textarea class="form-control" id="Razon_social" name="Razon_social" rows="2" cols="50" maxlength="99" placeholder="Razón social ..." required></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="RFC" class="col-md-2 col-form-label">RFC: </label>
            <div class="col-10">
              <input class="form-control" type="text" name="RFC" id="RFC" maxlength="14" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Concepto" class="col-md-2 col-form-label">Concepto: </label>
            <div class="col-10">
              <input class="form-control" type="text" name="Concepto" id="Concepto" maxlength="30">
            </div>
          </div>

          <div class="form-group row">
            <label for="Monto" class="col-md-2 col-form-label">Monto: </label>
            <div class="col-10">
              <input class="form-control" type="number" name="Monto" id="Monto" step="0.01">
            </div>
          </div>

          <div class="form-group row">
            <label for="IVA" class="col-md-2 col-form-label">IVA: </label>
            <div class="col-md-4">
              <div class="input-group">
                <input class="form-control" type="number" id="porcentaje" name="porcentaje" value="16">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">%</span>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <input class="form-control" type="number" id="IVA" name="IVA" step="0.01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Monto" class="col-md-2 col-form-label">Salida: </label>
            <div class="col-md-5">
              <input class="form-control" type="number" id="Salida" name="Salida" step="0.01" required>
            </div>
            <div class="col-5">
              <button type="button" id="buttonff" class="btn btn-primary">Calcular</button>
            </div>
          </div>

          <div class="form-group row">
            <label for="Total" class="col-md-2 col-form-label">Banco: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio11" name="radio1" value="BBVA BANCOMER" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio11">BBVA BANCOMER</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio12" name="radio1" value="INBURSA" class="custom-control-input">
                   <label class="custom-control-label" for="radio12">INBURSA</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio13" name="radio1" value="SCOTIABANK" class="custom-control-input">
                   <label class="custom-control-label" for="radio13">SCOTIABANK</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio14" name="radio1" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio14">otro</label>
                   <input type="text" placeholder="Banco" name="banco" class="form-control form-control-sm" />
              </div>
            </div>
          </div>

          <div class="form-group row">
            <label for="radio" class="col-md-2 col-form-label">Página: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio01" name="radio" value="CEO" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio01">CEO</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio02" name="radio" value="GENIOS" class="custom-control-input">
                   <label class="custom-control-label" for="radio02">GENIOS</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio03">otro</label>
                   <input type="text" placeholder="Página" name="pag" class="form-control form-control-sm" />
              </div>
            </div>
          </div>

          <script>
          document.getElementById("buttonff").onclick = myFunctionff;
          function myFunctionff() {
            var porcentaje = parseFloat(document.getElementById("porcentaje").value)/100.0;
            var Monto = parseFloat(document.getElementById("Monto").value);
            var iva = parseFloat(Monto*porcentaje);
            document.getElementById("IVA").value = iva;
            document.getElementById("Salida").value = Monto + iva;
          }

          </script>


        <?php
        break;
      //-----------------------------------------------
      case 'importefondo': ?>
        <h2>Agregar elemento Importe Fondo fijo</h2>
        <form action="<?php echo base_url();?>index.php/welcome/agregar_importefondofijo" method="POST" enctype="multipart/form-data">

          <div class="form-group row">
            <label for="Fecha" class="col-md-2 col-form-label">Fecha:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="Fecha" id="Fecha" min="2000-01-01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Importe" class="col-md-2 col-form-label">Importe: </label>
            <div class="col-10">
              <input class="form-control" type="number" name="Importe" id="Importe" step="0.01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Total" class="col-md-2 col-form-label">Banco: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio11" name="radio1" value="BBVA BANCOMER" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio11">BBVA BANCOMER</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio12" name="radio1" value="INBURSA" class="custom-control-input">
                   <label class="custom-control-label" for="radio12">INBURSA</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio13" name="radio1" value="SCOTIABANK" class="custom-control-input">
                   <label class="custom-control-label" for="radio13">SCOTIABANK</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio14" name="radio1" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio14">otro</label>
                   <input type="text" placeholder="Banco" name="banco" class="form-control form-control-sm" />
              </div>
            </div>
          </div>

          <div class="form-group row">
            <label for="radio" class="col-md-2 col-form-label">Página: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio01" name="radio" value="CEO" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio01">CEO</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio02" name="radio" value="GENIOS" class="custom-control-input">
                   <label class="custom-control-label" for="radio02">GENIOS</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio03">otro</label>
                   <input type="text" placeholder="Página" name="pag" class="form-control form-control-sm" />
              </div>
            </div>
          </div>

        <?php
        break;
      //-----------------------------------------------
      case 'factura': ?>
        <h2>Agregar elemento Factura</h2>
        <form action="<?php echo base_url();?>index.php/welcome/agregar_facturas" id="factura" method="POST" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="Folio" class="col-md-2 col-form-label">Folio: </label>
            <div class="col-10">
              <input class="form-control" type="number" name="Folio" id="Folio" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Concepto" class="col-md-2 col-form-label">Concepto: </label>
            <div class="col-10">
              <textarea class="form-control" id="Concepto" name="Concepto" rows="3" cols="50" maxlength="199" placeholder="Concepto ..." required></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="Fecha" class="col-md-2 col-form-label">Fecha:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="Fecha" id="Fecha" min="2000-01-01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Importe" class="col-md-2 col-form-label">Importe: </label>
            <div class="col-md-5">
              <input class="form-control" type="number" id="Importe" name="Importe" step="0.01">
            </div>
            <div class="col-5">
              <button type="button" id="button1" class="btn btn-primary">Calcular</button>
            </div>
          </div>

          <div class="form-group row">
            <label for="Retenciones" class="col-md-2 col-form-label">Retenciones: </label>
            <div class="col-10">
              <input class="form-control" type="number" id="Retenciones" name="Retenciones" step="0.01">
            </div>
          </div>

          <div class="form-group row">
            <label for="IVA" class="col-md-2 col-form-label">IVA: </label>
            <div class="col-md-4">
              <div class="input-group">
                <input class="form-control" type="number" id="porcentaje" name="porcentaje" value="16">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">%</span>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <input class="form-control" type="number" id="IVA" name="IVA" step="0.01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="moneda" class="col-md-2 col-form-label">Moneda: </label>
            <div class="col-10">
              <select class="custom-select" id="moneda" name="moneda" form="factura">
                <option value="MXN">$ MXN</option>
                <option value="USD">$ USD</option>
                <option value="EUR">€ EUR</option>
                <option value="GBP">£ GBP</option>
                <option value="JPY">¥ JPY</option>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label for="Total" class="col-md-2 col-form-label">Total: </label>
            <div class="col-10">
              <input class="form-control" type="number" id="Total" name="Total" step="0.01">
            </div>
          </div>

          <div class="form-group row">
            <label for="Razon_social" class="col-md-2 col-form-label">Razón social: </label>
            <div class="col-10">
              <textarea class="form-control" id="Razon_social" name="Razon_social" rows="3" cols="50" maxlength="100" placeholder="Razón social ..."></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="_social" class="col-md-2 col-form-label">Estado: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="Estado0" name="Estado" value="ACTIVA" class="custom-control-input" checked>
                   <label class="custom-control-label" for="Estado0">ACTIVA</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="Estado1" name="Estado" value="CANCELADA" class="custom-control-input">
                   <label class="custom-control-label" for="Estado1">CANCELADA</label>
              </div>
            </div>
          </div>

          <div class="form-group row">
            <label for="radio" class="col-md-2 col-form-label">Página: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio01" name="radio" value="CEO" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio01">CEO</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio02" name="radio" value="GENIOS" class="custom-control-input">
                   <label class="custom-control-label" for="radio02">GENIOS</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio03">otro</label>
                   <input type="text" placeholder="Página" name="pag" class="form-control form-control-sm" />
              </div>
            </div>
          </div>

          <script>
          document.getElementById("button1").onclick = myFunction;
          function myFunction() {
            var porcentaje = parseFloat(document.getElementById("porcentaje").value)/100.0;
            var importe = parseFloat(document.getElementById("Importe").value);
            var iva = parseFloat(importe*porcentaje);
            document.getElementById("IVA").value = iva;
            document.getElementById("Total").value = importe + iva;
          }
          </script>

        <?php
        break;
      //-----------------------------------------------
      case 'nomina': ?>
        <h2>Agregar elemento Nómina</h2>
        <form action="<?php echo base_url();?>index.php/welcome/agregar_nomina" method="POST" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="Folio" class="col-md-2 col-form-label">Folio: </label>
            <div class="col-10">
              <input class="form-control" type="number" name="Folio" id="Folio" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Periodo" class="col-md-2 col-form-label">Periodo: </label>
            <div class="col-10">
              <input class="form-control" type="text" name="Periodo" id="Periodo" maxlength="20">
            </div>
          </div>

          <div class="form-group row">
            <label for="Fecha" class="col-md-2 col-form-label">Fecha:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="Fecha" id="Fecha" min="2000-01-01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Importe" class="col-md-2 col-form-label">Importe: </label>
            <div class="col-10">
              <input class="form-control" type="number" id="Importe" name="Importe" step="0.01">
            </div>
          </div>

          <div class="form-group row">
            <label for="IMSS" class="col-md-2 col-form-label">IMSS: </label>
            <div class="col-10">
              <input class="form-control" type="number" id="IMSS" name="IMSS" step="0.01">
            </div>
          </div>

          <div class="form-group row">
            <label for="Subsidio" class="col-md-2 col-form-label">Subsidio: </label>
            <div class="col-10">
              <input class="form-control" type="number" id="Subsidio" name="Subsidio" step="0.01">
            </div>
          </div>

          <div class="form-group row">
            <label for="ISR" class="col-md-2 col-form-label">ISR: </label>
            <div class="col-10">
              <input class="form-control" type="number" id="ISR" name="ISR" step="0.01">
            </div>
          </div>

          <div class="form-group row">
            <label for="Empleado" class="col-md-2 col-form-label">Empleado: </label>
            <div class="col-10">
              <input class="form-control" type="text" id="Empleado" name="Empleado" maxlength="100">
            </div>
          </div>

          <div class="form-group row">
            <label for="radio" class="col-md-2 col-form-label">Página: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio01" name="radio" value="CEO" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio01">CEO</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio02" name="radio" value="GENIOS" class="custom-control-input">
                   <label class="custom-control-label" for="radio02">GENIOS</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio03">otro</label>
                   <input type="text" placeholder="Página" name="pag" class="form-control form-control-sm" />
              </div>
            </div>
          </div>
        <?php
        break;
      //-----------------------------------------------
      case 'cpp': ?>
        <h2>Agregar elemento Cuentas por pagar</h2>
        <form action="<?php echo base_url();?>index.php/welcome/agregar_cpp" method="POST" enctype="multipart/form-data">

          <div class="form-group row">
            <label for="Fecha" class="col-md-2 col-form-label">Fecha:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="Fecha" id="Fecha" min="2000-01-01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Concepto" class="col-md-2 col-form-label">Concepto: </label>
            <div class="col-10">
              <textarea class="form-control" id="Concepto" name="Concepto" rows="2" cols="50" maxlength="99" placeholder="Concepto ..." required></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="Empresa" class="col-md-2 col-form-label">Empresa: </label>
            <div class="col-10">
              <textarea class="form-control" id="Empresa" name="Empresa" rows="2" cols="50" maxlength="99" placeholder="Empresa ..." required></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="Importe" class="col-md-2 col-form-label">Importe: </label>
            <div class="col-10">
              <input class="form-control" type="number" id="Importe" name="Importe" step="0.01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Nota" class="col-md-2 col-form-label">Nota: </label>
            <div class="col-10">
              <textarea class="form-control" id="Nota" name="Nota" rows="3" cols="50" maxlength="199" placeholder="Nota ..." required></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="Estado" class="col-md-2 col-form-label">Estado: </label>
            <div class="col-10">
              <textarea class="form-control" id="Estado" name="Estado" rows="2" cols="50" maxlength="99" placeholder="Estado ..." required></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="radio" class="col-md-2 col-form-label">Página: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio01" name="radio" value="CEO" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio01">CEO</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio02" name="radio" value="GENIOS" class="custom-control-input">
                   <label class="custom-control-label" for="radio02">GENIOS</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio03">otro</label>
                   <input type="text" placeholder="Página" name="pag" class="form-control form-control-sm" />
              </div>
            </div>
          </div>
        <?php
        break;
      //-----------------------------------------------
      case 'cpc': ?>
        <h2>Agregar elemento Cuentas por cobrar</h2>
        <form action="<?php echo base_url();?>index.php/welcome/agregar_cpc" method="POST" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="Concepto" class="col-md-2 col-form-label">Concepto: </label>
            <div class="col-10">
              <textarea class="form-control" id="Concepto" name="Concepto" rows="2" cols="50" maxlength="99" placeholder="Concepto ..."></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="Empresa" class="col-md-2 col-form-label">Empresa: </label>
            <div class="col-10">
              <textarea class="form-control" id="Empresa" name="Empresa" rows="2" cols="50" maxlength="99" placeholder="Empresa ..." required></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="Cantidad" class="col-md-2 col-form-label">Cantidad: </label>
            <div class="col-10">
              <input class="form-control" type="number" id="Cantidad" name="Cantidad" step="0.01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Fecha_limite" class="col-md-2 col-form-label">Fecha limite:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="Fecha_limite" id="Fecha_limite" min="2000-01-01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Nota" class="col-md-2 col-form-label">Nota: </label>
            <div class="col-10">
              <textarea class="form-control" id="Nota" name="Nota" rows="3" cols="50" maxlength="199" placeholder="Nota ..." required></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="Estatus" class="col-md-2 col-form-label">Estatus: </label>
            <div class="col-10">
              <textarea class="form-control" id="Estatus" name="Estatus" rows="2" cols="50" maxlength="99" placeholder="Estatus ..." required></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="radio" class="col-md-2 col-form-label">Página: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio01" name="radio" value="CEO" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio01">CEO</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio02" name="radio" value="GENIOS" class="custom-control-input">
                   <label class="custom-control-label" for="radio02">GENIOS</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio03">otro</label>
                   <input type="text" placeholder="Página" name="pag" class="form-control form-control-sm" />
              </div>
            </div>
          </div>
        <?php
        break;
      //-----------------------------------------------
      case 'servicios': ?>
        <h2>Agregar elemento Servicios</h2>
        <form action="<?php echo base_url();?>index.php/welcome/agregar_servicios" method="POST" enctype="multipart/form-data" id="servicio">

          <div class="form-group row">
            <label for="" class="col-md-2 col-form-label">Tipo de Servicio: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio11" name="radio2" value="Telefonia" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio11">Telefonía e Internet(TOTALPLAY/AXTEL/TELMEX)</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio12" name="radio2" value="Impresiones" class="custom-control-input">
                   <label class="custom-control-label" for="radio12">Impresiones</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio13" name="radio2" value="Celulares" class="custom-control-input">
                   <label class="custom-control-label" for="radio13">Celulares(TELCEL/AT&T/MOVISTAR)</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio14" name="radio2" value="Luz" class="custom-control-input">
                   <label class="custom-control-label" for="radio14">Luz(CFE)</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio15" name="radio2" value="Renta local" class="custom-control-input">
                   <label class="custom-control-label" for="radio15">Renta local</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio16" name="radio2" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio16">otro</label>
                   <input type="text" placeholder="Servicio" name="ser" class="form-control form-control-sm" />
              </div>
            </div>
          </div>

          <div class="form-group row">
            <label for="service" class="col-md-2 col-form-label">Servicio: </label>
            <div class="col-10">
              <select class="custom-select" id="service" name="service" form="servicio">
                <option value="TOTALPLAY">TOTALPLAY</option>
                <option value="AXTEL">AXTEL</option>
                <option value="TELMEX">TELMEX</option>
                <option value="DAEN">DAEN</option>
                <option value="TELCEL">TELCEL</option>
                <option value="AT&T">AT&T</option>
                <option value="MOVISTAR">MOVISTAR</option>
                <option value="CFE">CFE</option>
                <option value="Renta local">Renta local</option>
                <option value="OTRO">otro</option>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label for="Folio" class="col-md-2 col-form-label">Folio: </label>
            <div class="col-10">
              <input class="form-control" type="text" name="Folio" id="Folio" maxlength="20" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Fecha_emision" class="col-md-2 col-form-label">Fecha emisión:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="Fecha_emision" id="Fecha_emision" min="2000-01-01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Fecha_pago" class="col-md-2 col-form-label">Fecha pago:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="Fecha_pago" id="Fecha_pago" min="2000-01-01">
            </div>
          </div>

          <div class="form-group row">
            <label for="Forma_pago" class="col-md-2 col-form-label">Forma pago: </label>
            <div class="col-10">
              <select class="custom-select" id="Forma_pago" name="Forma_pago" form="servicio">
                <option value="Efectivo">Efectivo</option>
                <option value="Tarjeta Debito">Tarjeta Débito</option>
                <option value="Tarjeta Crédito">Tarjeta Crédito</option>
                <option value="Transferencia Bancaria">Transferencia Bancaria</option>
                <option value="Ingreso Bancario">Ingreso Bancario</option>
                <option value="Paypal">Paypal</option>
                <option value="Direct Debit">Direct Debit</option>
                <option value="Otro">Otro</option>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label for="Periodo" class="col-md-2 col-form-label">Periodo: </label>
            <div class="col-10">
              <input class="form-control" type="text" name="Periodo" id="Periodo" maxlength="20" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Importe" class="col-md-2 col-form-label">Importe: </label>
            <div class="col-10">
              <input class="form-control" type="number" id="Importe" name="Importe" step="0.01">
            </div>
          </div>

          <div class="form-group row">
            <label for="radio" class="col-md-2 col-form-label">Página: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio01" name="radio" value="CEO" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio01">CEO</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio02" name="radio" value="GENIOS" class="custom-control-input">
                   <label class="custom-control-label" for="radio02">GENIOS</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio03">otro</label>
                   <input type="text" placeholder="Página" name="pag" class="form-control form-control-sm" />
              </div>
            </div>
          </div>
        <?php
        break;
      //-----------------------------------------------
      case 'sat': ?>
        <h2>Agregar elemento SAT</h2>
        <form action="<?php echo base_url();?>index.php/welcome/agregar_sat" method="POST" enctype="multipart/form-data">

          <div class="form-group row">
            <label for="mes" class="col-md-2 col-form-label">Mes: </label>
            <div class="col-10">
              <select class="custom-select" id="mes" name="mes">
                <option value="ENERO">ENERO</option>
                <option value="FEBRERO">FEBRERO</option>
                <option value="MARZO">MARZO</option>
                <option value="ABRIL">ABRIL</option>
                <option value="MAYO">MAYO</option>
                <option value="JUNIO">JUNIO</option>
                <option value="JULIO">JULIO</option>
                <option value="AGOSTO">AGOSTO</option>
                <option value="SEPTIEMBRE">SEPTIEMBRE</option>
                <option value="OCTUBRE">OCTUBRE</option>
                <option value="NOVIEMBRE">NOVIEMBRE</option>
                <option value="DICIEMBRE">DICIEMBRE</option>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label for="anio" class="col-md-2 col-form-label">Año: </label>
            <div class="col-10">
              <input class="form-control" type="number" id="anio" name="anio" min="2000" max="2050" placeholder="Año" required>
            </div>
          </div>

          <fieldset>
            <br />
            <legend>Normal:</legend>
            <div class="form-group row">
              <label for="Ntotal" class="col-md-2 col-form-label">Total: </label>
              <div class="col-10">
                <input class="form-control" type="number" id="Ntotal" name="Ntotal" step="0.01" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="Nfpre" class="col-md-2 col-form-label">Fecha Presentación:</label>
              <div class="col-10">
                <input class="form-control" type="date" name="Nfpre" id="Nfpre" min="2000-01-01" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="Nfpag" class="col-md-2 col-form-label">Fecha Pago:</label>
              <div class="col-10">
                <input class="form-control" type="date" name="Nfpag" id="Nfpag" min="2000-01-01">
              </div>
            </div>
        </fieldset>

        <fieldset>
          <br />
          <legend>Complementario 1:</legend>
          <div class="form-group row">
            <label for="C1total" class="col-md-2 col-form-label">Total: </label>
            <div class="col-10">
              <input class="form-control" type="number" id="C1total" name="C1total" step="0.01">
            </div>
          </div>
          <div class="form-group row">
            <label for="C1fpre" class="col-md-2 col-form-label">Fecha Presentación:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="C1fpre" id="C1fpre" min="2000-01-01">
            </div>
          </div>
          <div class="form-group row">
            <label for="C1fpag" class="col-md-2 col-form-label">Fecha Pago:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="C1fpag" id="C1fpag" min="2000-01-01">
            </div>
          </div>
        </fieldset>

        <fieldset>
          <br />
          <legend>Complementario 2:</legend>
          <div class="form-group row">
            <label for="C2total" class="col-md-2 col-form-label">Total: </label>
            <div class="col-10">
              <input class="form-control" type="number" id="C2total" name="C2total" step="0.01">
            </div>
          </div>
          <div class="form-group row">
            <label for="C2fpre" class="col-md-2 col-form-label">Fecha Presentación:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="C2fpre" id="C2fpre" min="2000-01-01">
            </div>
          </div>
          <div class="form-group row">
            <label for="C2fpag" class="col-md-2 col-form-label">Fecha Pago:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="C2fpag" id="C2fpag" min="2000-01-01">
            </div>
          </div>
      </fieldset>

      <fieldset>
        <br />
        <legend>Complementario 3:</legend>
        <div class="form-group row">
          <label for="C3total" class="col-md-2 col-form-label">Total: </label>
          <div class="col-10">
            <input class="form-control" type="number" id="C3total" name="C3total" step="0.01">
          </div>
        </div>
        <div class="form-group row">
          <label for="C3fpre" class="col-md-2 col-form-label">Fecha Presentación:</label>
          <div class="col-10">
            <input class="form-control" type="date" name="C3fpre" id="C3fpre" min="2000-01-01">
          </div>
        </div>
        <div class="form-group row">
          <label for="C3fpag" class="col-md-2 col-form-label">Fecha Pago:</label>
          <div class="col-10">
            <input class="form-control" type="date" name="C3fpag" id="C3fpag" min="2000-01-01">
          </div>
        </div>
      </fieldset>

      <fieldset>
        <br />
        <legend>Complementario 4:</legend>
        <div class="form-group row">
          <label for="C4total" class="col-md-2 col-form-label">Total: </label>
          <div class="col-10">
            <input class="form-control" type="number" id="C4total" name="C4total" step="0.01">
          </div>
        </div>
        <div class="form-group row">
          <label for="C4fpre" class="col-md-2 col-form-label">Fecha Presentación:</label>
          <div class="col-10">
            <input class="form-control" type="date" name="C4fpre" id="C4fpre" min="2000-01-01">
          </div>
        </div>
        <div class="form-group row">
          <label for="C4fpag" class="col-md-2 col-form-label">Fecha Pago:</label>
          <div class="col-10">
            <input class="form-control" type="date" name="C4fpag" id="C4fpag" min="2000-01-01">
          </div>
        </div>
      </fieldset>

      <div class="form-group row">
        <label for="radio" class="col-md-2 col-form-label">Página: </label>
        <div class="col-10">
          <div class="custom-control custom-radio">
               <input type="radio" id="radio01" name="radio" value="CEO" class="custom-control-input" checked>
               <label class="custom-control-label" for="radio01">CEO</label>
          </div>
          <div class="custom-control custom-radio">
               <input type="radio" id="radio02" name="radio" value="GENIOS" class="custom-control-input">
               <label class="custom-control-label" for="radio02">GENIOS</label>
          </div>
          <div class="custom-control custom-radio">
               <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
               <label class="custom-control-label" for="radio03">otro</label>
               <input type="text" placeholder="Página" name="pag" class="form-control form-control-sm" />
          </div>
        </div>
      </div>


        <?php
        break;
      //-----------------------------------------------
      case 'requerimiento': ?>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
        <script src="<?php echo base_url(); ?>static\jquery\jquery-1.12.4.js"></script>
        <script src="<?php echo base_url(); ?>static\jquery\jquery-ui.js"></script>
        <script src="<?php echo base_url(); ?>static\jquery\autocomplete.js"></script>

        <h2>Agregar elemento Requerimiento</h2>
        <form action="<?php echo base_url();?>index.php/welcome/agregar_requerimientos" method="POST" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="Fecha" class="col-md-2 col-form-label">Fecha:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="Fecha" id="Fecha" min="2000-01-01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="inputentidad" class="col-md-2 col-form-label">Entidad: </label>
            <div class="col-10">
              <input class="form-control" type="text" id="inputentidad" name="inputentidad" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="inputtipodoc" class="col-md-2 col-form-label">Tipo de documento: </label>
            <div class="col-10">
              <input class="form-control" type="text" id="inputtipodoc" name="inputtipodoc" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="inputtiposol" class="col-md-2 col-form-label">Solicitud: </label>
            <div class="col-10">
              <input class="form-control" type="text" id="inputtiposol" name="inputtiposol" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Folio" class="col-md-2 col-form-label">Folio: </label>
            <div class="col-10">
              <input class="form-control" type="text" name="Folio" id="Folio" maxlength="20" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Comentarios" class="col-md-2 col-form-label">Comentarios: </label>
            <div class="col-10">
              <textarea class="form-control" id="Comentarios" name="Comentarios" rows="2" cols="50" maxlength="99" placeholder="Comentarios ..."></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="radio" class="col-md-2 col-form-label">Página: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio01" name="radio" value="CEO" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio01">CEO</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio02" name="radio" value="GENIOS" class="custom-control-input">
                   <label class="custom-control-label" for="radio02">GENIOS</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio03">otro</label>
                   <input type="text" placeholder="Página" name="pag" class="form-control form-control-sm" />
              </div>
            </div>


        <?php
        break;
      //-----------------------------------------------
      case 'controlinterno': ?>
        <h2>Agregar elemento Control interno</h2>
        <form action="<?php echo base_url();?>index.php/welcome/agregar_controlinterno" method="POST" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="Fecha" class="col-md-2 col-form-label">Fecha:</label>
            <div class="col-10">
              <input class="form-control" type="date" name="Fecha" id="Fecha" min="2000-01-01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Cantidad" class="col-md-2 col-form-label">Cantidad: </label>
            <div class="col-10">
              <input class="form-control" type="number" id="Cantidad" name="Cantidad" step="0.01" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Mutuante" class="col-md-2 col-form-label">Mutuante: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="Mutuante01" name="Mutuante" value="Pedro Hernández Téllez" class="custom-control-input" checked>
                   <label class="custom-control-label" for="Mutuante01">Pedro Hernández Téllez</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="Mutuante02" name="Mutuante" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="Mutuante02">otro</label>
                   <input type="text" placeholder="Mutuante" name="persona1" class="form-control form-control-sm" />
              </div>
            </div>
          </div>

          <div class="form-group row">
            <label for="Mutuario" class="col-md-2 col-form-label">Mutuario: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="Mutuario01" name="Mutuario" value="Pedro Hernández Téllez" class="custom-control-input" checked>
                   <label class="custom-control-label" for="Mutuario01">Pedro Hernández Téllez</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="Mutuario02" name="Mutuario" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="Mutuario02">otro</label>
                   <input type="text" placeholder="Mutuario" name="persona2" class="form-control form-control-sm" />
              </div>
            </div>
          </div>

          <div class="form-group row">
            <label for="doc" class="col-md-2 col-form-label">Tipo de documento: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="doc01" name="doc" value="Contrato de mutuo" class="custom-control-input" checked>
                   <label class="custom-control-label" for="doc01">Contrato de mutuo</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="doc02" name="doc" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="doc02">otro</label>
                   <input type="text" placeholder="Tipo de documento" name="tipodoc" class="form-control form-control-sm" />
              </div>
            </div>
          </div>

          <div class="form-group row">
            <label for="radio" class="col-md-2 col-form-label">Página: </label>
            <div class="col-10">
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio01" name="radio" value="CEO" class="custom-control-input" checked>
                   <label class="custom-control-label" for="radio01">CEO</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio02" name="radio" value="GENIOS" class="custom-control-input">
                   <label class="custom-control-label" for="radio02">GENIOS</label>
              </div>
              <div class="custom-control custom-radio">
                   <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
                   <label class="custom-control-label" for="radio03">otro</label>
                   <input type="text" placeholder="Página" name="pag" class="form-control form-control-sm" />
              </div>
            </div>
          </div>

        <?php
        break;
      //-----------------------------------------------
      case 'cheques': ?>
      <h2>Agregar elemento Cheques</h2>
      <form action="<?php echo base_url();?>index.php/welcome/agregar_cheques" method="POST" enctype="multipart/form-data">
        <div class="form-group row">
          <label for="Fecha" class="col-md-2 col-form-label">Fecha:</label>
          <div class="col-10">
            <input class="form-control" type="date" name="Fecha" id="Fecha" min="2000-01-01" required>
          </div>
        </div>

        <div class="form-group row">
          <label for="Cheque" class="col-md-2 col-form-label">Cheque: </label>
          <div class="col-10">
            <input class="form-control" type="number" id="Cheque" name="Cheque" step="0.01" required>
          </div>
        </div>

        <div class="form-group row">
          <label for="Cliente" class="col-md-2 col-form-label">Cliente: </label>
          <div class="col-10">
            <input class="form-control" type="text" id="Cliente" name="Cliente" maxlength="50" required>
          </div>
        </div>

        <div class="form-group row">
          <label for="Subtotal" class="col-md-2 col-form-label">Subtotal: </label>
          <div class="col-10">
            <input class="form-control" type="number" id="Subtotal" name="Subtotal" step="0.01" required>
          </div>
        </div>

        <div class="form-group row">
          <label for="Total" class="col-md-2 col-form-label">Total: </label>
          <div class="col-10">
            <input class="form-control" type="number" id="Total" name="Total" step="0.01" required>
          </div>
        </div>

        <div class="form-group row">
          <label for="Total" class="col-md-2 col-form-label">Banco: </label>
          <div class="col-10">
            <div class="custom-control custom-radio">
                 <input type="radio" id="radio11" name="radio1" value="BBVA BANCOMER" class="custom-control-input" checked>
                 <label class="custom-control-label" for="radio11">BBVA BANCOMER</label>
            </div>
            <div class="custom-control custom-radio">
                 <input type="radio" id="radio12" name="radio1" value="INBURSA" class="custom-control-input">
                 <label class="custom-control-label" for="radio12">INBURSA</label>
            </div>
            <div class="custom-control custom-radio">
                 <input type="radio" id="radio13" name="radio1" value="SCOTIABANK" class="custom-control-input">
                 <label class="custom-control-label" for="radio13">SCOTIABANK</label>
            </div>
            <div class="custom-control custom-radio">
                 <input type="radio" id="radio14" name="radio1" value="otro" class="custom-control-input">
                 <label class="custom-control-label" for="radio14">otro</label>
                 <input type="text" placeholder="Banco" name="banco" class="form-control form-control-sm" />
            </div>
          </div>
        </div>

        <div class="form-group row">
          <label for="radio" class="col-md-2 col-form-label">Status: </label>
          <div class="col-10">
            <div class="custom-control custom-radio">
                 <input type="radio" id="radio01" name="radio" value="COBRADO" class="custom-control-input" checked>
                 <label class="custom-control-label" for="radio01">COBRADO</label>
            </div>
            <div class="custom-control custom-radio">
                 <input type="radio" id="radio02" name="radio" value="CANCELADO" class="custom-control-input">
                 <label class="custom-control-label" for="radio02">CANCELADO</label>
            </div>
            <div class="custom-control custom-radio">
                 <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
                 <label class="custom-control-label" for="radio03">otro</label>
                 <input type="text" placeholder="Status" name="pag" class="form-control form-control-sm" />
            </div>
          </div>
        </div>


        <?php
        break;

      default:
        // code...
        break;
    }
    ?>

    <div class="row pt-4">
     <button type="submit" id="submit" name="submit" class="btn btn-primary btn-lg btn-block">Agregar</button>
    </div>
  </form>
</div>
</section>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

    <?php } else
   redirect('/Welcome/index/', 'refresh');
 ?>
</body>

<body>
<?php if(($this->session->userdata('nivel')>='1') &&   ($this->session->userdata('nivel')<='2')){?>

<!-- seccion para generar grafica -->
<section class="container">
  <br>
  <label>Sección para gráfica</label>
<?php
if ($opc>0){

  if($opc==1){ ?>
    <form action="<?php echo base_url();?>index.php/welcome/graficaServicio" method="POST">
  <?php
  }else{
  ?>
    <form action="<?php echo base_url();?>index.php/welcome/obtenergraficaSAT" method="POST">
  <?php
  }
  ?>
  <div class="container-fluid">
  <div class="form-row align-items-center justify-content-start">
    <div class="col-md-3">
      <label>Página</label><br />
      <div class="custom-control custom-radio">
           <input type="radio" id="radio01" name="radio" value="CEO" class="custom-control-input" checked>
           <label class="custom-control-label" for="radio01">CEO</label>
      </div>
      <div class="custom-control custom-radio">
           <input type="radio" id="radio02" name="radio" value="GENIOS" class="custom-control-input">
           <label class="custom-control-label" for="radio02">GENIOS</label>
      </div>
      <div class="custom-control custom-radio">
           <input type="radio" id="radio03" name="radio" value="otro" class="custom-control-input">
           <label class="custom-control-label" for="radio03">otro</label>
           <input type="text" placeholder="Página" name="pag" class="form-control form-control-sm" />
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label><br>Seleccione Año</label>
        <div class="input-group mb-3">
         <div class="input-group-prepend">
           <label class="input-group-text" for="inputGroupSelect01">Año: </label>
         </div>
         <select class="custom-select" id="year" name="year">
           <?php
           if($opc==1){
             if($years!=FALSE){
              foreach($years->result() as $fila) { ?>
              <option value="<?=$fila->year?>"><?=$fila ->year?></option><?php }
            }else{ ?><option value="">No se encontraron años</option><?php }
           }else{
             if($years!=FALSE){
              foreach($years->result() as $fila) { ?>
              <option value="<?=$fila->Año?>"><?=$fila ->Año?></option><?php }
            }else{ ?><option value="">No se encontraron años</option><?php }
           } ?>
         </select>
        </div>

        <label><br>Seleccione tipo de gráfica</label>
        <div class="input-group mb-3">
         <div class="input-group-prepend">
           <label class="input-group-text" for="inputGroupSelect01">Gráfica: </label>
         </div>
         <select class="custom-select tipo" id="tipo" name="tipo">
           <option value="line">Línea</option>
           <option value="bar">Barra</option>
           <option value="pie">Pastel</option>
           <option value="doughnut">Dona</option>
           <option value="polarArea">Polar area</option>
         </select>
        </div>

      </div>
    </div>
    <div class="col-md-3">
    <?php
    if($opc==1){
    ?>
    <label>Servicio</label><br />
    <div class="custom-control custom-radio">
         <input type="radio" id="radio11" name="radio2" value="Telefonia" class="custom-control-input" checked>
         <label class="custom-control-label" for="radio11">Telefonía e Internet(TOTALPLAY/AXTEL/TELMEX)</label>
    </div>
    <div class="custom-control custom-radio">
         <input type="radio" id="radio12" name="radio2" value="Impresiones" class="custom-control-input">
         <label class="custom-control-label" for="radio12">Impresiones</label>
    </div>
    <div class="custom-control custom-radio">
         <input type="radio" id="radio13" name="radio2" value="Celulares" class="custom-control-input">
         <label class="custom-control-label" for="radio13">Celulares(TELCEL/AT&T/MOVISTAR)</label>
    </div>
    <div class="custom-control custom-radio">
         <input type="radio" id="radio14" name="radio2" value="Luz" class="custom-control-input">
         <label class="custom-control-label" for="radio14">Luz(CFE)</label>
    </div>
    <div class="custom-control custom-radio">
         <input type="radio" id="radio15" name="radio2" value="Renta local" class="custom-control-input">
         <label class="custom-control-label" for="radio15">Renta local</label>
    </div>
    <div class="custom-control custom-radio">
         <input type="radio" id="radio16" name="radio2" value="otro" class="custom-control-input">
         <label class="custom-control-label" for="radio16">otro</label>
         <input type="text" placeholder="Servicio" name="ser" class="form-control form-control-sm" />
    </div>

    <?php
    }
    ?>
    </div>

    <div class="col-md-2 align-self-start" >
     <div class="form-group text-right">
      <button type="submit button" id="submit" name="submit" class="btn btn-primary">Graficar</button>
     </div>
    </div>

  </div>
</div>
</form>
</section>

<?php
}
?>

     <?php } else
   redirect('/Welcome/index/', 'refresh');
 ?>
</body>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'/third_party/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class Welcome extends CI_Controller {

    function __construct()
    {
     parent::__construct();
     $this->load->model('bases_model');
     $this->load->library('grocery_CRUD');
     $this->load->library('encrypt');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//$this->load->view('welcome_message');
        $this->load->view('head');
        $this->load->view('login');
        // $this->load->view('footer');
	}

  /*public function validate_captcha(){
    $captcha=$this->input->post('g-recaptcha-response');
    $response=@file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lcx7TAUAAAAAOqusOB9xnvf_mbmR31Ha_491FOS & response=".$captcha."& remoteip=".$_SERVER['REMOTE_ADDR']);
    if ($response.'success'==FALSE)
      return FALSE;
    else
      return TRUE;
  }*/

  public function validaLogin(){

   /*
      $encrypted_string = $this->encrypt->encode($this->input->post('pwd'));
      var_dump($encrypted_string);
      var_dump($this->encrypt->decode($encrypted_string));
      var_dump($this->input->post('login'));

   */

    $this->form_validation->set_rules('login','login','trim|required');
    $this->form_validation->set_rules('pwd','pwd','trim|required');
    //$this->form_validation->set_rules('g-recaptcha-response','recaptcha validation','trim|required|callback_validate_captcha');
    $this->form_validation->set_message('required', '<div class="alert alert-danger">El campo %s es  obligatorio</div>');
    if ($this->form_validation->run() != FALSE)
    {
      $data = array(
               'login'=> $this->input->post('login'),'password'=>$this->input->post('pwd'));
      $usuarios = $this->bases_model->validaLogin($data);
       if ($usuarios == FALSE)
         redirect('/Welcome/index/', 'location');
      else{
        $datasession="";
        foreach($usuarios->result() as $row){
            $datasession = array(
            'login'=> $row->login,
            'password'=> $row->password,
            'nombre'=> $row->nombre,
            'nivel'=> $row->nivel,);
        }
        $this->session->set_userdata($datasession);
        $this->load->view('head');
        $this->load->view('encabezado');
        $this->load->view('home',$data);
        $this->load->view('footer');
     }
    }else{
      $this->load->view('head');
      $this->load->view('login');
      $this->load->view('footer');
    }
  }

  public function cerrarSesion()
  {
   $datasession = array('nivel' => '');
      $this->session->unset_userdata($datasession);
     $this->session->sess_destroy();
     redirect('/Welcome/index/', 'refresh');
   }

    public function bienvenida(){
      $this->load->view('head');
      $this->load->view('encabezado');
      $this->load->view('home');
      $this->load->view('footer');
    }

    public function configButtonGC($crud,$ver=0){
      if($this->session->userdata('nivel') == '1'){
        $crud->unset_clone();
        $crud->unset_add();
      }
      else{
          $crud->unset_clone();
          $crud->unset_add();
          $crud->unset_delete();
          $crud->unset_edit();
          if ($ver!=1){
            $crud->unset_read();
          }
      }
      return $crud;
    }

    public function usuarios(){
      if($this->session->userdata('nivel') == '1'){
        try{
         $crud = new grocery_CRUD();
         $crud->set_table('usuarios');
         $crud->columns('login','password','nombre','nivel');
         $crud->set_theme('flexigrid');
         $crud->required_fields('login','password','nombre','nivel');
         $crud->display_as('login','Usuario');
         $crud->display_as('password','Contraseña');
         $crud->display_as('nombre','Nombre');
         $crud->set_subject('Registro');

         $crud->unset_clone();

         $output = $crud->render();
         $this->_example_output($output,0);
        }catch(Exception $e){
         show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
      }else{
        header('Location: bienvenida');
      }

    }

    public function caja()
    {
      try{
        $crud = new grocery_CRUD();
        $crud->set_table('cajachica');
        $crud->columns('Fecha','Concepto','Entrada','Salida','Saldo','Pagina');
        $crud->set_theme('flexigrid');
        $crud->required_fields('Fecha','Pagina');
        $crud->display_as('Pagina','Página');
        $crud->set_subject('Registro');

        $crud = $this->configButtonGC( $crud );

        $output = $crud->render();
        $this->_example_output($output,1);
      }catch(Exception $e){
        show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }
    }

    public function banco(){
      try{
       $crud = new grocery_CRUD();
       $crud->set_table('bancos');
       $crud->columns('Fecha','Descripcion','Referencia','Cargos','Abonos','Estatus_comprobacion', 'Banco', 'Pagina');
       $crud->set_theme('flexigrid');
       $crud->required_fields('Fecha','Banco', 'Pagina');
       $crud->display_as('Descripcion','Descripción');
       $crud->display_as('Estatus_comprobacion','Estatus Comprobación');
       $crud->display_as('Pagina','Página');
       $crud->set_subject('Registro');

       $temp = &$crud;

       $crud = $this->configButtonGC( $crud,1 );

       $output = $crud->render();
       $this->_example_output($output,2);
      }catch(Exception $e){
       show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }
    }

    public function fondoFijo(){
      try{
       $crud = new grocery_CRUD();
       $crud->set_table('fondofijo');
       $crud->columns('Fecha','Folio','Razon_social','RFC','Concepto','Monto', 'IVA', 'Salida', 'Saldo', 'Banco', 'Pagina');
       $crud->set_theme('flexigrid');
       $crud->required_fields('Fecha','Folio','Razon_social','RFC','Monto', 'IVA', 'Salida', 'Saldo', 'Banco', 'Pagina');
       $crud->display_as('Razon_social','Razón social');
       $crud->display_as('Pagina','Página');
       $crud->set_subject('Registro');

       $crud = $this->configButtonGC( $crud,1 );

       $output = $crud->render();
       $this->_example_output($output,3);
      }catch(Exception $e){
       show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }
    }

    public function impfondoFijo(){
      try{
       $crud = new grocery_CRUD();
       $crud->set_table('importefondofijo');
       $crud->columns('Fecha','Importe','Banco','Pagina','Mes','Anio');
       $crud->set_theme('flexigrid');
       $crud->required_fields('Fecha','Importe','Banco','Pagina','Mes','Anio');
       $crud->display_as('Pagina','Página');
       $crud->set_subject('Registro');

       $crud = $this->configButtonGC( $crud,1 );

       $output = $crud->render();
       $this->_example_output($output,3.5);
      }catch(Exception $e){
       show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }
    }

    public function facturas(){
      try{
       $crud = new grocery_CRUD();
       $crud->set_table('facturas');
       $crud->columns('Folio','Fecha','Concepto','Razon_social','Importe','Retenciones','IVA', 'Total', 'Moneda','Estado','Pagina','Mes','Anio');
       $crud->set_theme('flexigrid');
       $crud->required_fields('Folio','Concepto','Estado','Pagina','Mes');
       //$crud->unset_fields('Anio');//quitar campos de editar
       $crud->display_as('Razon_social','Razón social');
       $crud->display_as('Anio','Año');
       $crud->display_as('Pagina','Página');
       $crud->set_subject('Registro');

       $crud = $this->configButtonGC( $crud,1 );

       $output = $crud->render();
       $this->_example_output($output,4);
      }catch(Exception $e){
       show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }
    }

    public function nomina(){
      try{
       $crud = new grocery_CRUD();
       $crud->set_table('nomina');
       $crud->columns('Folio','Fecha','Periodo','IMSS','Importe','Subsidio','ISR','Empleado','Pagina');
       $crud->set_theme('flexigrid');
       $crud->required_fields('Folio','Pagina');
       $crud->display_as('Pagina','Página');
       $crud = $this->configButtonGC( $crud,1 );

       $output = $crud->render();
       $this->_example_output($output,5);
      }catch(Exception $e){
       show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }
    }

    public function cpp(){
      try{
       $crud = new grocery_CRUD();
       $crud->set_table('cpp');
       $crud->columns('Fecha','Concepto','Empresa','Importe','Nota','Estado','Pagina');
       $crud->set_theme('flexigrid');
       $crud->required_fields('Fecha','Concepto','Empresa','Importe','Nota','Estado','Pagina');
       $crud->display_as('Pagina','Página');
       $crud = $this->configButtonGC( $crud );

       $output = $crud->render();
       $this->_example_output($output,6);
      }catch(Exception $e){
       show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }
    }

    public function cpc(){
      try{
       $crud = new grocery_CRUD();
       $crud->set_table('cpc');
       $crud->columns('Concepto','Empresa','Cantidad','Fecha_limite','Nota','Estatus','Pagina');
       $crud->set_theme('flexigrid');
       $crud->required_fields('Cantidad','Fecha_limite','Estatus','Pagina');
       $crud->display_as('Fecha_limite','Fecha limite');
       $crud->display_as('Pagina','Página');
       $crud = $this->configButtonGC( $crud );

       $output = $crud->render();
       $this->_example_output($output,7);
      }catch(Exception $e){
       show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }
    }

    public function servicios($gg=0){
        $g=$gg;
      try{
       $crud = new grocery_CRUD();
       $crud->set_table('servicios');
       $crud->columns('Tipo','Servicio','Folio','Fecha_emision','Fecha_pago','Forma_pago','Periodo','Importe','Pagina');
       $crud->set_theme('flexigrid');
       $crud->required_fields('Tipo','Servicio','Folio','Fecha_emision','Pagina');
       $crud->display_as('Tipo','Servicio');
       $crud->display_as('Servicio','Empresa');
       $crud->display_as('Fecha_emision','Fecha emisión');
       $crud->display_as('Fecha_pago','Fecha pago');
       $crud->display_as('Pagina','Página');
       $crud = $this->configButtonGC( $crud );

       $output['tabla'] = $crud->render();
       $output['g']=$g;
       $this->_example_output($output,8);
      }catch(Exception $e){
       show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }
    }

    public function sat($gg=0){

      $g=$gg;
      try{
       $crud = new grocery_CRUD();
       $crud->set_table('sat_nuevo');
       $crud->columns('Mes','Año','Ntotal','Nfpre','Nfpag','C1total','C1fpre','C1fpag','C2total','C2fpre','C2fpag','C3total','C3fpre','C3fpag','C4total','C4fpre','C4fpag','Pagina');
       $crud->set_theme('flexigrid');
       $crud->required_fields('Mes','Año','Ntotal','Nfpre','Pagina');
       $crud->display_as('Ntotal','Total');
       $crud->display_as('Nfpre','Presentación');
       $crud->display_as('Nfpag','Pago');
       $crud->display_as('C1total','C1 Total');
       $crud->display_as('C1fpre','C1 Presentación');
       $crud->display_as('C1fpag','C1 Pago');
       $crud->display_as('C2total','C2 Total');
       $crud->display_as('C2fpre','C2 Presentación');
       $crud->display_as('C2fpag','C2 Pago');
       $crud->display_as('C3total','C3 Total');
       $crud->display_as('C3fpre','C3 Presentación');
       $crud->display_as('C3fpag','C3 Pago');
       $crud->display_as('C4total','C4 Total');
       $crud->display_as('C4fpre','C4 Presentación');
       $crud->display_as('C4fpag','C4 Pago');
       $crud->display_as('Pagina','Página');
       $crud = $this->configButtonGC( $crud,1 );

       $output['tabla'] = $crud->render();
       $output['g']=$g;
       $this->_example_output($output,9);
      }catch(Exception $e){
       show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }
    }

    public function requerimientos(){
      try{
       $crud = new grocery_CRUD();
       $crud->set_table('requerimientos');
       $crud->columns('Fecha','Entidad','Documento','Solicitud','Folio','Comentarios','Pagina');
       $crud->set_theme('flexigrid');
       $crud->required_fields('Fecha','Entidad','Documento','Solicitud','Folio','Pagina');
       $crud->display_as('Pagina','Página');

       $crud = $this->configButtonGC( $crud,1 );

       $output = $crud->render();
       $this->_example_output($output,10);
      }catch(Exception $e){
       show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }
    }

    public function controlIntterno(){
      try{
       $crud = new grocery_CRUD();
       $crud->set_table('control_interno');
       $crud->columns('Fecha','Cantidad','Empresa','Contratista','Documento','Pagina');
       $crud->set_theme('flexigrid');
       $crud->required_fields('Fecha','Cantidad','Empresa','Contratista','Documento','Pagina');
       $crud->display_as('Empresa','Mutuario');
       $crud->display_as('Contratista','Mutuante');
       $crud->display_as('Pagina','Página');

       $crud = $this->configButtonGC( $crud );

       $output = $crud->render();
       $this->_example_output($output,11);
      }catch(Exception $e){
       show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }
    }

    public function cheques(){
      try{
       $crud = new grocery_CRUD();
       $crud->set_table('cheques');
       $crud->columns('Fecha','Banco','Cheque','Cliente','Subtotal','Total','Estado');
       $crud->set_theme('flexigrid');
       $crud->required_fields('Fecha','Banco','Cheque','Estado');

       $crud = $this->configButtonGC( $crud );

       $output = $crud->render();
       $this->_example_output($output,12);
      }catch(Exception $e){
       show_error($e->getMessage().' --- '.$e->getTraceAsString());
      }
    }

    public function _example_output($output = null,$boton = 0)
    {
      $this->load->view('head');
      $this->load->view('encabezado');
      switch ($boton) {
        //array('seccion',pag,banco,Mes/Pag/ninguno,Año,importaBoton)
        //seccion,pag,banco,año (1 mostrar, 0 no mostrar)
        //importarboton (1 mostrar) (0 no mostrar)
        //mostrar mes (0), mostrar pag (1)
        case 0:
          $data['datos']=array('usuario',0,0,0,0,0);
          $this->load->view('importarArchivo',$data );
          break;
        case 1:
          $data['datos']=array('caja',0,0,0,0,1);
          $this->load->view('importarArchivo',$data );
          break;
        case 2:
          $data['datos']=array('banco',1,1,0,0,1);
          $this->load->view('importarArchivo',$data );
          break;
        case 3:
          $data['datos']=array('fondo',1,1,0,1,1);
          // $data['Saldo']=$this->bases_model->getMinSaldoff1();
          $this->load->view('importarArchivo',$data );
          break;
        case 3.5:
          $data['datos']=array('importefondo',0,0,0,0,0);
          $this->load->view('importarArchivo',$data );
          break;
        case 4:
          $data['datos']=array('factura',1,0,0,1,1);
          $this->load->view('importarArchivo',$data );
          break;
        case 5:
          $data['datos']=array('nomina',1,0,0,0,1);
          $this->load->view('importarArchivo',$data );
          break;
        case 6:
          $data['datos']=array('cpp',1,0,2,0,1);
          $this->load->view('importarArchivo',$data );
          break;
        case 7:
          $data['datos']=array('cpc',1,0,2,0,1);
          $this->load->view('importarArchivo',$data );
          break;
        case 8:
          $data['datos']=array('servicios',0,0,0,1,1);
          $this->load->view('importarArchivo',$data );

          $data['years']=$this->bases_model->getServicioyear();
          $data['opc']=1;
          $this->load->view('graficar',$data);
          break;
        case 9:
          $data['datos']=array('sat',0,0,1,0,1);
          $this->load->view('importarArchivo',$data );

          $data['years']=$this->bases_model->getSATyear();
          $data['opc']=2;
          $this->load->view('graficar',$data);
          break;
        case 10:
          $data['datos']=array('requerimiento',1,0,1,0,1);
          $this->load->view('importarArchivo',$data );
          break;
        case 11:
          $data['datos']=array('controlinterno',1,0,1,0,1);
          $this->load->view('importarArchivo',$data );
          break;
        case 12:
          $data['datos']=array('cheques',0,0,2,0,1);
          $this->load->view('importarArchivo',$data );
          break;
        default:
          break;
      }
      //para el footer tanto en sat como en sat grafica se coloco una bandera
      if($boton==9 || $boton==8){
          if($output['g']==0){
            $this->load->view('example.php',(array)$output['tabla']);
                $this->load->view('footer');
          }else {
              $this->load->view('example.php',(array)$output['tabla']);
          }
      }else {
        $this->load->view('example.php',(array)$output);
            $this->load->view('footer');
      }

    }


        public function iframe($op=0,$tipo=null,$year=null,$pag=null,$servicio=null){
          //op=(1 para grafica sat y 2 para grafica servicio)
          //recibe los valores y hace la consulta
          //var_dump($servicio);
          switch ($op){
            case 1:
                    $graph=array(
                     'year'=>$year,
                     'pag'=>$pag);
                     $data['grafica']=$this->bases_model->getSAT($graph);
                     $data['tipo'] = $tipo;
                     $data['year']= $year;
                     if($data['grafica']!=FALSE){
                       $this->load->view('head');
                       $this->load->view('graficasat',$data);
                     }else {
                        // echo "no se puede mostrar la grafica";
                        echo "<script>alert('no se puede mostrar la grafica');</script>";
                     }
              break;

            case 2:
                     $graph=array(
                      'servicio'=>$servicio,
                      'pag'=>$pag,
                       'year'=>$year);
                     $data['grafica']=$this->bases_model->getServicio($graph);
                     $data['tipo'] = $tipo;
                     $data['year']= $year;
                     $data['ser']=$servicio;
                     if($data['grafica']!=FALSE){
                       $this->load->view('head');
                       $this->load->view('graficaser',$data);
                     }else {
                        // echo "no se puede mostrar la grafica";
                        echo "<script>alert('no se puede mostrar la grafica');</script>";
                     }
              break;
              default:
                break;
          }
        }//fin-Iframe



    public function obtenergraficaSAT(){
      //recibe los parametros y los manda a iframe
      $year=$this->input->post('year');
      $pag = $this->getPagina($this->input->post('radio'));
      $tipo =$this->input->post('tipo');
        $data['tipo'] = $tipo;
        $data['year'] = $year;
        $data['pag'] = $pag;
        $data['op']=1;
        $this->sat(1);
        $this->load->view('canvas',$data);
        $this->load->view('footer');


    }//Finde obtenergrafica

    public function graficaServicio(){
        //recibe los parametros y los manda a iframe
      $year=$this->input->post('year');
      $tipo =$this->input->post('tipo');
      $pag = $this->getPagina($this->input->post('radio'));
      $ser = $this->getServicio($this->input->post('radio2'));
        $data['tipo'] = $tipo;
        $data['ser']= $ser;
        $data['year']= $year;
        $data['pag']= $pag;
        $data['op']=2;
        $this->servicios(1);
        $this->load->view('canvas',$data);
        $this->load->view('footer');

    }//FindegraficaServicio


    public function getServicio($pag){
      /*regresa la pagina seleccionada por un radio-button*/
      if (isset($_POST['submit'])) {
        if(isset($_POST['radio2'])){
          if ($_POST['radio2']=="otro"){
            $banco=$this->input->post('ser');
          }
          else {
            $pag=$_POST['radio2'];
          }
        }
        else{
          echo "<span>Please choose any radio button.</span>";
        }
      }
      return $pag;
    }


    public function getPagina($pag){
      /*regresa la pagina seleccionada por un radio-button*/
      if (isset($_POST['submit'])) {
        if(isset($_POST['radio'])){
          if ($_POST['radio']=="otro"){
            $pag=strtoupper ($this->input->post('pag'));
          }
          else {
            $pag=$_POST['radio'];
          }
        }
        else{
          echo "<span>Please choose any radio button.</span>";
        }
      }
      return $pag;
    }

    public function getBanco($banco){
      /*regresa el banco seleccionado por un radio-button*/
      if (isset($_POST['submit'])) {
        if(isset($_POST['radio1'])){
          if ($_POST['radio1']=="otro"){
            $banco=strtoupper ($this->input->post('banco'));
          }
          else {
            $banco=$_POST['radio1'];
          }
        }
        else{
          echo "<span>Please choose any radio button.</span>";
        }
      }
      return $banco;
    }

    public function getMutuante($Mutuante){
      /*regresa el Mutante seleccionado por un radio-button*/
      if (isset($_POST['submit'])) {
        if(isset($_POST['Mutuante'])){
          if ($_POST['Mutuante']=="otro"){
            $Mutuante=strtoupper ($this->input->post('persona1'));
          }
          else {
            $Mutuante=$_POST['Mutuante'];
          }
        }
        else{
          echo "<span>Please choose any radio button.</span>";
        }
      }
      return $Mutuante;
    }

    public function getMutuario($Mutuario){
      /*regresa el Mutante seleccionado por un radio-button*/
      if (isset($_POST['submit'])) {
        if(isset($_POST['Mutuario'])){
          if ($_POST['Mutuario']=="otro"){
            $Mutuario=strtoupper ($this->input->post('persona2'));
          }
          else {
            $Mutuario=$_POST['Mutuario'];
          }
        }
        else{
          echo "<span>Please choose any radio button.</span>";
        }
      }
      return $Mutuario;
    }

    public function getdoc($doc){
      /*regresa el Mutante seleccionado por un radio-button*/
      if (isset($_POST['submit'])) {
        if(isset($_POST['doc'])){
          if ($_POST['doc']=="otro"){
            $doc=strtoupper ($this->input->post('tipodoc'));
          }
          else {
            $doc=$_POST['doc'];
          }
        }
        else{
          echo "<span>Please choose any radio button.</span>";
        }
      }
      return $doc;
    }

    public function contenidoCeldas($row,$opc,$otros){
      /* Recorre el contenido completo de una fila
      row=fila
      opc=opcion para regresar el array (1=cajachica,2=banco,...)
      otros=otros datos
      Regresa un array con los datos de la fila
      */
      for ($i=0; $i<count($row); $i++){
        if ($row[$i]!=""){
          // echo("<script>console.log('>>> ".$i." ".$row[$i]."');</script>");
          switch ($opc) {
            case 'cajachica':
              //Compara que el elemento de la fila sea una fecha
              if (DateTime::createFromFormat('d/m/Y', $row[$i]) !== FALSE) {
                $data = array(
                  'Fecha' => $row[$i],
                  'Concepto' => $row[$i+1],
                  'Entrada' => $row[$i+2],
                  'Salida' => $row[$i+3],
                  'Saldo' => $row[$i+4],
                  'Pagina' => $row[$i+5]
                  // 'Pagina' =>$otros[0]
               );
               return $data;
              }
              break;
            case 'banco':
              //Compara que el elemento de la fila sea una fecha
              if (DateTime::createFromFormat('d/m/Y', $row[$i]) !== FALSE) {
                $data = array(
                  'Fecha' => $row[$i],
                  'Descripcion' => $row[$i+1],
                  'Referencia' => $row[$i+2],
                  'Cargos' => $row[$i+3],
                  'Abonos' => $row[$i+4],
                  'Estatus_comprobacion' => $row[$i+5],
                  'Banco' => $otros[0],
                  'Pagina' => $otros[1]
               );
               return $data;
              }
              break;
            case 'fondofijo1':
              if (DateTime::createFromFormat('d/m/Y', $row[$i]) !== FALSE && (is_string($row[$i+1]) || is_int($row[$i+1])) && (is_float($row[$i+5]) || is_int($row[$i+5])) ) {
                $data = array(
                  'Fecha' => $row[$i],
                  'Folio' => $row[$i+1],
                  'Razon_social' => $row[$i+2],
                  'RFC' => $row[$i+3],
                  'Concepto' => $row[$i+4],
                  'Monto' => $row[$i+5],
                  'IVA' => $row[$i+6],
                  'Salida' => $row[$i+7],
                  'Saldo' => $row[$i+8],
                  'Pagina' => $otros[0],
                  'Banco' => $otros[1],
                  'Mes' => $otros[2],
                  'Anio' => $otros[3]
               );
               return $data;
              }
              break;
              case 'fondofijo2':
                if ((is_float($row[$i]) || is_int($row[$i])) && DateTime::createFromFormat('d/m/Y', $row[$i+1]) !== FALSE && is_string($row[$i+2]) ) {
                  $data = array(
                    'Importe' => $row[$i],
                    'Fecha' => $row[$i+1],
                    'Banco' => $row[$i+2],
                    'Pagina' => $otros[0],
                    'Mes' => $otros[1],
                    'Anio' => $otros[2]
                 );
                 return $data;
                }
                break;
            case 'factura':
              if (is_int($row[$i]) && is_string($row[$i+1]) && is_string($row[$i+9]) ) {
                //var_dump ($row[$i],$row[$i+1],strcmp($row[$i+1],"NOMINA"));
                $data = array(
                  'Folio' => $row[$i],
                  'Concepto' => $row[$i+1],
                  'Fecha' => $row[$i+2],
                  'Importe' => $row[$i+3],
                  'Retenciones' => $row[$i+4],
                  'IVA' => $row[$i+5],
                  // 'moneda' => "",
                  // 'Total' => $row[$i+6],
                  // 'Razon_social' => $row[$i+7],
                  // 'Estado' => $row[$i+9],
                  'moneda' => $row[$i+6],
                  'Total' => $row[$i+7],
                  'Razon_social' => $row[$i+8],
                  'Estado' => $row[$i+9],
                  'Pagina' => $otros[0],
                  'Mes' => $otros[1],
                  'Anio' => $otros[2]
               );
               return $data;
              }//if is_int
              break;
            case 'nomina':
              if (is_int($row[$i]) && is_string($row[$i+1]) ) {
                //var_dump ($row[$i],$row[$i+1],strcmp($row[$i+1],"NOMINA"));
                $data = array(
                  'Folio' => $row[$i],
                  'Periodo' => $row[$i+1],
                  'Fecha' => $row[$i+2],
                  'Importe' => $row[$i+3],
                  'IMSS' => (is_string($row[$i+4]) ? 0 : $row[$i+4]),
                  'Subsidio' => (is_string($row[$i+5]) ? 0 : $row[$i+5]),
                  'ISR' => (is_string($row[$i+6]) ? 0 : $row[$i+6]),
                  'Empleado' => $row[$i+7],
                  'Pagina' => $otros[0]
               );
               return $data;
              }//if is_int
              break;
            case 'cpp':
              if (DateTime::createFromFormat('d/m/Y', $row[$i]) !== FALSE) {
                $data = array(
                  'Fecha' => $row[$i],
                  'Concepto' => $row[$i+1],
                  'Empresa' => $row[$i+2],
                  'Importe' => $row[$i+3],
                  'Nota' => $row[$i+4],
                  'Estado' => $row[$i+5],
                  'Pagina' => $otros[0]
               );
               return $data;
              }
              break;
            case 'cpc':
              if (DateTime::createFromFormat('d/m/Y', $row[$i+3]) !== FALSE) {
                $data = array(
                  'Concepto' => $row[$i],
                  'Empresa' => $row[$i+1],
                  'Cantidad' => $row[$i+2],
                  'Fecha_limite' => $row[$i+3],
                  'Nota'=> $row[$i+4],
                  'Estatus'=> $row[$i+5],
                  'Pagina' => $otros[0]
               );
               return $data;
              }
              break;
            case 'servicio':
              if (is_string($row[$i]) && is_string($row[$i+1])  && (DateTime::createFromFormat('d/m/Y', $row[$i+4]) !== FALSE) ) {
                //var_dump ($row[$i],$row[$i+1],$row[$i+2]);
                $data = array(
                  'Tipo' => $row[$i],
                  'Servicio' => $row[$i+1],
                  'Folio' => $row[$i+2],
                  'Periodo' => $row[$i+3],
                  'Fecha_emision' => $row[$i+4],
                  'Importe'=> $row[$i+5],
                  'Fecha_pago' => $row[$i+6],
                  'Forma_pago' => $row[$i+7],
                  'Pagina' => $row[$i+8]
               );
               return $data;
              }
              break;
            case 'sat':
              if (is_string($row[$i]) && is_int($row[$i+1]) && (DateTime::createFromFormat('d/m/Y', $row[$i+3]) !== FALSE) ) {
                // var_dump ($row[$i],$row[$i+1],$row[$i+2]);
                $data = array(
                  'Mes' => $row[$i],
                  'Año' => $row[$i+1],
                  'Ntotal' => $row[$i+2],
                  'Nfpre' => $row[$i+3],
                  'Nfpag' => $row[$i+4],
                  'C1total'=> $row[$i+5],
                  'C1fpre'=> $row[$i+6],
                  'C1fpag'=> $row[$i+7],
                  'C2total' => $row[$i+8],
                  'C2fpre' => $row[$i+9],
                  'C2fpag'=> $row[$i+10],
                  'C3total'=> $row[$i+11],
                  'C3fpre'=> $row[$i+12],
                  'C3fpag'=> $row[$i+13],
                  'C4total'=> $row[$i+14],
                  'C4fpre'=> $row[$i+15],
                  'C4fpag'=> $row[$i+16],
                  'Pagina' => $otros[0]
               );
               return $data;
              }
              break;
            case 'requerimiento':
              //Compara que el elemento de la fila sea una fecha
              if (DateTime::createFromFormat('d/m/Y', $row[$i]) !== FALSE) {
                $data = array(
                  'Fecha' => $row[$i],
                  'Entidad' => $row[$i+1],
                  'Documento' => $row[$i+2],
                  'Solicitud' => $row[$i+3],
                  'Folio' => $row[$i+4],
                  'Comentarios' => $row[$i+5],
                  'Pagina' => $otros[0]
               );
               return $data;
              }
              break;
            case 'controlinterno':
              if (DateTime::createFromFormat('d/m/Y', $row[$i]) !== FALSE) {
                $data = array(
                  'Fecha' => $row[$i],
                  'Cantidad' => $row[$i+1],
                  'Empresa' => $row[$i+2],
                  'Contratista' => $row[$i+3],
                  'Documento' => $row[$i+4],
                  'Pagina' => $otros[0]
               );
               return $data;
              }
              break;
              case 'cheques':
                if ((DateTime::createFromFormat('d/m/Y', $row[$i]) !== FALSE) && is_string($row[$i+1]) &&  is_int($row[$i+2])) {
                  $data = array(
                    'Fecha' => $row[$i],
                    'Banco' => $row[$i+1],
                    'Cheque' => $row[$i+2],
                    'Cliente' => $row[$i+3],
                    'Subtotal' => $row[$i+4],
                    'Total' => $row[$i+5],
                    'Estado'=> $row[$i+6]
                 );
                 return $data;
                }
                break;

            default:
              // code...
              break;
          }//switch
        }//if vacio""
      }//for
      //si la fila esta vacia regresa null
      return ;
    }

    public function guardar_excel_caja(){
      $pag = $this->getPagina($this->input->post('radio'));
      $mes=$this->input->post('mes');

       if (!empty($_FILES['file']['name'])) {
        $pathinfo = pathinfo($_FILES["file"]["name"]);
         if (($pathinfo['extension'] == 'xls' || $pathinfo['extension'] == 'xlsx')
             && $_FILES['file']['size'] > 0 ) {

          // Nombre Temporal del Archivo
          $inputFileName = $_FILES['file']['tmp_name'];
          //Lee el Archivo usando ReaderFactory
          $reader = ReaderFactory::create(Type::XLSX);

          //Esta linea mantiene el formato de nuestras horas y fechas
          //Sin esta linea Spout convierte la hora y fecha a su propio formato
          //predefinido como DataTime
          $reader->setShouldFormatDates(true);

          // Abrimos el archivo
          $reader->open($inputFileName);
          $count = 1;

          //Numero de Hojas en el Archivo
          foreach ($reader->getSheetIterator() as $sheet) {
            //solo libro del mes
            if( strtoupper($sheet->getName() ) == $mes){
              // Numero de filas en el documento EXCEL
              foreach ($sheet->getRowIterator() as $row) {
                $data = $this->contenidoCeldas( $row, 'cajachica', array($pag) );
                if ($data != null ){
                  $this->bases_model->insertTablaCajaChica($data);
                }
                $count++;
               }
             }
           }
           // cerramos el archivo EXCEL
            $reader->close();
            redirect('/Welcome/caja/', 'refresh');
         } else {
          // echo "Seleccione un tipo de Archivo Valido";
          echo "<script>alert('Seleccione un tipo de Archivo Valido');</script>";
          redirect('/Welcome/caja/', 'refresh');
         }
       } else {
       //echo "Seleccione un Archivo EXCEL";
       redirect('/Welcome/caja/', 'refresh');
      }
    }//guardar_excel_caja

    public function guardar_excel_banco(){
      $banco = $this->getBanco($this->input->post('radio1'));
      $pag = $this->getPagina($this->input->post('radio'));
      $mes=$this->input->post('mes');
       if (!empty($_FILES['file']['name'])) {
        $pathinfo = pathinfo($_FILES["file"]["name"]);
         if (($pathinfo['extension'] == 'xls' || $pathinfo['extension'] == 'xlsx')
             && $_FILES['file']['size'] > 0 ) {

          // Nombre Temporal del Archivo
          $inputFileName = $_FILES['file']['tmp_name'];
          //Lee el Archivo usando ReaderFactory
          $reader = ReaderFactory::create(Type::XLSX);

          //Esta linea mantiene el formato de nuestras horas y fechas
          //Sin esta linea Spout convierte la hora y fecha a su propio formato
          //predefinido como DataTime
          $reader->setShouldFormatDates(true);

          // Abrimos el archivo
          $reader->open($inputFileName);
          $count = 1;
          //Numero de Hojas en el Archivo
          foreach ($reader->getSheetIterator() as $sheet) {
            //solo libro del mes
            if( strtoupper($sheet->getName() ) == $mes){
              // Numero de filas en el documento EXCEL
              foreach ($sheet->getRowIterator() as $row) {
                 $data = $this->contenidoCeldas( $row, 'banco', array($banco,$pag) );
                 if ($data != null ){
                   $this->bases_model->insertTablaBanco($data);
                 }
                $count++;
               }
             }
           }
           // cerramos el archivo EXCEL
            $reader->close();
            redirect('/Welcome/banco/', 'refresh');
         } else {
          echo "<script>alert('Seleccione un tipo de Archivo Valido');</script>";
          redirect('/Welcome/banco/', 'refresh');
         }
       } else {
       //echo "Seleccione un Archivo EXCEL";
         redirect('/Welcome/banco/', 'refresh');
      }
    }//guardar_excel

    public function guardar_excel_fondofijo(){
      $banco = $this->getBanco($this->input->post('radio1'));
      $pag = $this->getPagina($this->input->post('radio'));
      $mes=$this->input->post('mes');
      $anio=$this->input->post('anio');
       if (!empty($_FILES['file']['name'])) {
        $pathinfo = pathinfo($_FILES["file"]["name"]);
         if (($pathinfo['extension'] == 'xls' || $pathinfo['extension'] == 'xlsx')
             && $_FILES['file']['size'] > 0 ) {

          // Nombre Temporal del Archivo
          $inputFileName = $_FILES['file']['tmp_name'];
          //Lee el Archivo usando ReaderFactory
          $reader = ReaderFactory::create(Type::XLSX);

          //Esta linea mantiene el formato de nuestras horas y fechas
          //Sin esta linea Spout convierte la hora y fecha a su propio formato
          //predefinido como DataTime
          $reader->setShouldFormatDates(true);

          // Abrimos el archivo
          $reader->open($inputFileName);
          $count = 1;
          //Numero de Hojas en el Archivo
          foreach ($reader->getSheetIterator() as $sheet) {
            //solo libro del mes
            if( strtoupper($sheet->getName() ) == $mes){
              // Numero de filas en el documento EXCEL
              foreach ($sheet->getRowIterator() as $row) {
                 $data = $this->contenidoCeldas( $row, 'fondofijo1', array($pag,$banco,$mes,$anio) );
                 if ($data != null ){
                   $this->bases_model->insertTablaFondofijo1($data);
                 }
                 else{
                   $data = $this->contenidoCeldas( $row, 'fondofijo2', array($pag,$mes,$anio) );
                   if ($data != null ){
                     $this->bases_model->insertTablaFondofijo2($data);
                   }
                 }
                $count++;
               }
             }
           }
           // cerramos el archivo EXCEL
            $reader->close();
            redirect('/Welcome/fondoFijo/', 'refresh');
         } else {
          echo "<script>alert('Seleccione un tipo de Archivo Valido');</script>";
          redirect('/Welcome/fondoFijo/', 'refresh');
         }
       } else {
       //echo "Seleccione un Archivo EXCEL";
         redirect('/Welcome/fondoFijo/', 'refresh');
      }
    }//guardar_excel

    public function guardar_excel_facturas(){
      $pag = $this->getPagina($this->input->post('radio'));
      $mes=$this->input->post('mes');
      $anio=$this->input->post('anio');
       if (!empty($_FILES['file']['name'])) {
        $pathinfo = pathinfo($_FILES["file"]["name"]);
         if (($pathinfo['extension'] == 'xls' || $pathinfo['extension'] == 'xlsx')
             && $_FILES['file']['size'] > 0 ) {

          // Nombre Temporal del Archivo
          $inputFileName = $_FILES['file']['tmp_name'];
          //Lee el Archivo usando ReaderFactory
          $reader = ReaderFactory::create(Type::XLSX);

          //Esta linea mantiene el formato de nuestras horas y fechas
          //Sin esta linea Spout convierte la hora y fecha a su propio formato
          //predefinido como DataTime
          $reader->setShouldFormatDates(true);

          // Abrimos el archivo
          $reader->open($inputFileName);
          $count = 1;
          //Numero de Hojas en el Archivo
          foreach ($reader->getSheetIterator() as $sheet) {
            //solo libro del mes
            if( strtoupper($sheet->getName() ) == $mes){
              // Numero de filas en el documento EXCEL
              foreach ($sheet->getRowIterator() as $row) {
                 $data = $this->contenidoCeldas( $row, 'factura', array($pag,$mes,$anio) );
                 if ($data != null ){
                   $this->bases_model->insertTablaFactura($data);
                 }
                $count++;
               }
             }
           }
           // cerramos el archivo EXCEL
            $reader->close();
            redirect('/Welcome/facturas/', 'refresh');
         } else {
          echo "<script>alert('Seleccione un tipo de Archivo Valido');</script>";
          redirect('/Welcome/facturas/', 'refresh');
         }
       } else {
       //echo "Seleccione un Archivo EXCEL";
         redirect('/Welcome/facturas/', 'refresh');
      }
    }//guardar_excel

    public function guardar_excel_nomina(){
      $pag = $this->getPagina($this->input->post('radio'));
      $mes=$this->input->post('mes');
       if (!empty($_FILES['file']['name'])) {
        $pathinfo = pathinfo($_FILES["file"]["name"]);
         if (($pathinfo['extension'] == 'xls' || $pathinfo['extension'] == 'xlsx')
             && $_FILES['file']['size'] > 0 ) {

          // Nombre Temporal del Archivo
          $inputFileName = $_FILES['file']['tmp_name'];
          //Lee el Archivo usando ReaderFactory
          $reader = ReaderFactory::create(Type::XLSX);

          //Esta linea mantiene el formato de nuestras horas y fechas
          //Sin esta linea Spout convierte la hora y fecha a su propio formato
          //predefinido como DataTime
          $reader->setShouldFormatDates(true);

          // Abrimos el archivo
          $reader->open($inputFileName);
          $count = 1;
          //Numero de Hojas en el Archivo
          foreach ($reader->getSheetIterator() as $sheet) {
            //solo libro del mes
            if( strtoupper($sheet->getName() ) == $mes){
              // Numero de filas en el documento EXCEL
              foreach ($sheet->getRowIterator() as $row) {
                 $data = $this->contenidoCeldas( $row, 'nomina', array($pag,$mes) );
                 if ($data != null ){
                   $this->bases_model->insertTablaNomina($data);
                 }
                $count++;
               }
             }
           }
           // cerramos el archivo EXCEL
            $reader->close();
            redirect('/Welcome/nomina/', 'refresh');
         } else {
          echo "<script>alert('Seleccione un tipo de Archivo Valido');</script>";
          redirect('/Welcome/nomina/', 'refresh');
         }
       } else {
       //echo "Seleccione un Archivo EXCEL";
         redirect('/Welcome/nomina/', 'refresh');
      }
    }//guardar_excel

    public function guardar_excel_cpp(){
      $pag = $this->getPagina($this->input->post('radio'));
      $mes=$this->input->post('mes');
       if (!empty($_FILES['file']['name'])) {
        $pathinfo = pathinfo($_FILES["file"]["name"]);
         if (($pathinfo['extension'] == 'xls' || $pathinfo['extension'] == 'xlsx')
             && $_FILES['file']['size'] > 0 ) {

          // Nombre Temporal del Archivo
          $inputFileName = $_FILES['file']['tmp_name'];
          //Lee el Archivo usando ReaderFactory
          $reader = ReaderFactory::create(Type::XLSX);

          //Esta linea mantiene el formato de nuestras horas y fechas
          //Sin esta linea Spout convierte la hora y fecha a su propio formato
          //predefinido como DataTime
          $reader->setShouldFormatDates(true);

          // Abrimos el archivo
          $reader->open($inputFileName);
          $count = 1;
          //Numero de Hojas en el Archivo
          foreach ($reader->getSheetIterator() as $sheet) {
            //solo libro del mes
            if( strtoupper($sheet->getName() ) == $mes){
              // Numero de filas en el documento EXCEL
              foreach ($sheet->getRowIterator() as $row) {
                 $data = $this->contenidoCeldas( $row, 'cpp', array($pag) );
                 if ($data != null ){
                   $this->bases_model->insertTablaCPP($data);
                 }
                $count++;
               }
             }
           }
           // cerramos el archivo EXCEL
            $reader->close();
            redirect('/Welcome/cpp/', 'refresh');
         } else {
          echo "<script>alert('Seleccione un tipo de Archivo Valido');</script>";
          redirect('/Welcome/cpp/', 'refresh');
         }
       } else {
       //echo "Seleccione un Archivo EXCEL";
         redirect('/Welcome/cpp/', 'refresh');
      }
    }//guardar_excel

    public function guardar_excel_cpc(){
      $pag = $this->getPagina($this->input->post('radio'));
      $mes=$this->input->post('mes');
       if (!empty($_FILES['file']['name'])) {
        $pathinfo = pathinfo($_FILES["file"]["name"]);
         if (($pathinfo['extension'] == 'xls' || $pathinfo['extension'] == 'xlsx')
             && $_FILES['file']['size'] > 0 ) {

          // Nombre Temporal del Archivo
          $inputFileName = $_FILES['file']['tmp_name'];
          //Lee el Archivo usando ReaderFactory
          $reader = ReaderFactory::create(Type::XLSX);

          //Esta linea mantiene el formato de nuestras horas y fechas
          //Sin esta linea Spout convierte la hora y fecha a su propio formato
          //predefinido como DataTime
          $reader->setShouldFormatDates(true);

          // Abrimos el archivo
          $reader->open($inputFileName);
          $count = 1;
          //Numero de Hojas en el Archivo
          foreach ($reader->getSheetIterator() as $sheet) {
            //solo libro del mes
            if( strtoupper($sheet->getName() ) == $mes){
              // Numero de filas en el documento EXCEL
              foreach ($sheet->getRowIterator() as $row) {
                 $data = $this->contenidoCeldas( $row, 'cpc', array($pag) );
                 if ($data != null ){
                   $this->bases_model->insertTablaCPC($data);
                 }
                $count++;
               }
             }
           }
           // cerramos el archivo EXCEL
            $reader->close();
            redirect('/Welcome/cpc/', 'refresh');
         } else {
          echo "<script>alert('Seleccione un tipo de Archivo Valido');</script>";
          redirect('/Welcome/cpc/', 'refresh');
         }
       } else {
       //echo "Seleccione un Archivo EXCEL";
         redirect('/Welcome/cpc/', 'refresh');
      }
    }//guardar_excel

    public function guardar_excel_servicios(){
      //$pag = $this->getPagina($this->input->post('radio'));
      $anio=$this->input->post('anio');
      $mes=$this->input->post('mes');
      //$mes.=" ".$anio; //para buscar en hoja "mes año" (formato Salvador)

       if (!empty($_FILES['file']['name'])) {
        $pathinfo = pathinfo($_FILES["file"]["name"]);
         if (($pathinfo['extension'] == 'xls' || $pathinfo['extension'] == 'xlsx')
             && $_FILES['file']['size'] > 0 ) {

          // Nombre Temporal del Archivo
          $inputFileName = $_FILES['file']['tmp_name'];
          //Lee el Archivo usando ReaderFactory
          $reader = ReaderFactory::create(Type::XLSX);

          //Esta linea mantiene el formato de nuestras horas y fechas
          //Sin esta linea Spout convierte la hora y fecha a su propio formato
          //predefinido como DataTime
          $reader->setShouldFormatDates(true);

          // Abrimos el archivo
          $reader->open($inputFileName);
          $count = 1;
          //Numero de Hojas en el Archivo
          foreach ($reader->getSheetIterator() as $sheet) {
            //solo libro del mes
            if( strtoupper($sheet->getName() ) == $mes){
              // Numero de filas en el documento EXCEL
              foreach ($sheet->getRowIterator() as $row) {
                 $data = $this->contenidoCeldas( $row, 'servicio', 'hola' );
                 if ($data != null ){
                   $this->bases_model->insertTablaServicios($data);
                 }
                $count++;
               }
             }
           }
           // cerramos el archivo EXCEL
            $reader->close();
            redirect('/Welcome/servicios/', 'refresh');
         } else {
          echo "<script>alert('Seleccione un tipo de Archivo Valido');</script>";
          redirect('/Welcome/servicios/', 'refresh');
         }
       } else {
       //echo "Seleccione un Archivo EXCEL";
         redirect('/Welcome/servicios/', 'refresh');
      }
    }//guardar_excel

    public function guardar_excel_sat(){
      $mes=$this->input->post('mes');
       if (!empty($_FILES['file']['name'])) {
        $pathinfo = pathinfo($_FILES["file"]["name"]);
         if (($pathinfo['extension'] == 'xls' || $pathinfo['extension'] == 'xlsx')
             && $_FILES['file']['size'] > 0 ) {

          // Nombre Temporal del Archivo
          $inputFileName = $_FILES['file']['tmp_name'];
          //Lee el Archivo usando ReaderFactory
          $reader = ReaderFactory::create(Type::XLSX);

          //Esta linea mantiene el formato de nuestras horas y fechas
          //Sin esta linea Spout convierte la hora y fecha a su propio formato
          //predefinido como DataTime
          $reader->setShouldFormatDates(true);

          // Abrimos el archivo
          $reader->open($inputFileName);
          $count = 1;
          //Numero de Hojas en el Archivo
          foreach ($reader->getSheetIterator() as $sheet) {
            //solo libro del mes
            if( strtoupper($sheet->getName() ) == $mes){
              // Numero de filas en el documento EXCEL
              foreach ($sheet->getRowIterator() as $row) {
                 $data = $this->contenidoCeldas( $row, 'sat', array($mes) );
                 if ($data != null ){
                   $this->bases_model->insertTablaSat($data);
                 }
                $count++;
               }
             }
           }
           // cerramos el archivo EXCEL
            $reader->close();
            redirect('/Welcome/sat/', 'refresh');
         } else {
          echo "<script>alert('Seleccione un tipo de Archivo Valido');</script>";
          redirect('/Welcome/sat/', 'refresh');
         }
       } else {
       //echo "Seleccione un Archivo EXCEL";
         redirect('/Welcome/sat/', 'refresh');
      }
    }//guardar_excel

    public function guardar_excel_requerimientos(){
      $pag = $this->getPagina($this->input->post('radio'));
      $mes=$this->input->post('mes');
       if (!empty($_FILES['file']['name'])) {
        $pathinfo = pathinfo($_FILES["file"]["name"]);
         if (($pathinfo['extension'] == 'xls' || $pathinfo['extension'] == 'xlsx')
             && $_FILES['file']['size'] > 0 ) {

          // Nombre Temporal del Archivo
          $inputFileName = $_FILES['file']['tmp_name'];
          //Lee el Archivo usando ReaderFactory
          $reader = ReaderFactory::create(Type::XLSX);

          //Esta linea mantiene el formato de nuestras horas y fechas
          //Sin esta linea Spout convierte la hora y fecha a su propio formato
          //predefinido como DataTime
          $reader->setShouldFormatDates(true);

          // Abrimos el archivo
          $reader->open($inputFileName);
          $count = 1;
          //Numero de Hojas en el Archivo
          foreach ($reader->getSheetIterator() as $sheet) {
            //solo libro del mes
            if( strtoupper($sheet->getName() ) == $mes){
              // Numero de filas en el documento EXCEL
              foreach ($sheet->getRowIterator() as $row) {
                 $data = $this->contenidoCeldas( $row, 'requerimiento', array($pag) );
                 if ($data != null ){
                   $this->bases_model->insertTablaRequerimientos($data);
                 }
                $count++;
               }
             }
           }
           // cerramos el archivo EXCEL
            $reader->close();
            redirect('/Welcome/requerimientos/', 'refresh');
         } else {
          echo "<script>alert('Seleccione un tipo de Archivo Valido');</script>";
          redirect('/Welcome/requerimientos/', 'refresh');
         }
       } else {
       //echo "Seleccione un Archivo EXCEL";
         redirect('/Welcome/requerimientos/', 'refresh');
      }
    }//guardar_excel

    public function guardar_excel_controlinterno(){
      $pag = $this->getPagina($this->input->post('radio'));
      $mes=$this->input->post('mes');
       if (!empty($_FILES['file']['name'])) {
        $pathinfo = pathinfo($_FILES["file"]["name"]);
         if (($pathinfo['extension'] == 'xls' || $pathinfo['extension'] == 'xlsx')
             && $_FILES['file']['size'] > 0 ) {

          // Nombre Temporal del Archivo
          $inputFileName = $_FILES['file']['tmp_name'];
          //Lee el Archivo usando ReaderFactory
          $reader = ReaderFactory::create(Type::XLSX);

          //Esta linea mantiene el formato de nuestras horas y fechas
          //Sin esta linea Spout convierte la hora y fecha a su propio formato
          //predefinido como DataTime
          $reader->setShouldFormatDates(true);

          // Abrimos el archivo
          $reader->open($inputFileName);
          $count = 1;
          //Numero de Hojas en el Archivo
          foreach ($reader->getSheetIterator() as $sheet) {
            //solo libro del mes
            if( strtoupper($sheet->getName() ) == $mes){
              // Numero de filas en el documento EXCEL
              foreach ($sheet->getRowIterator() as $row) {
                 $data = $this->contenidoCeldas( $row, 'controlinterno', array($pag) );
                 if ($data != null ){
                   $this->bases_model->insertTablaControlinterno($data);
                 }
                $count++;
               }
             }
           }
           // cerramos el archivo EXCEL
            $reader->close();
            redirect('/Welcome/controlIntterno/', 'refresh');
         } else {
          echo "<script>alert('Seleccione un tipo de Archivo Valido');</script>";
          redirect('/Welcome/controlIntterno/', 'refresh');
         }
       } else {
       //echo "Seleccione un Archivo EXCEL";
         redirect('/Welcome/controlIntterno/', 'refresh');
      }
    }//guardar_excel

    public function guardar_excel_cheques(){
       if (!empty($_FILES['file']['name'])) {
        $pathinfo = pathinfo($_FILES["file"]["name"]);
         if (($pathinfo['extension'] == 'xls' || $pathinfo['extension'] == 'xlsx')
             && $_FILES['file']['size'] > 0 ) {
          $inputFileName = $_FILES['file']['tmp_name'];
          $reader = ReaderFactory::create(Type::XLSX);
          $reader->setShouldFormatDates(true);

          $reader->open($inputFileName);
          foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $row) {
               $data = $this->contenidoCeldas( $row, 'cheques', array() );
               if ($data != null ){
                 $this->bases_model->insertTablaCheques($data);
               }
             }
           }
           // cerramos el archivo EXCEL
            $reader->close();
            redirect('/Welcome/cheques/', 'refresh');
         } else {
          echo "<script>alert('Seleccione un tipo de Archivo Valido');</script>";
          redirect('/Welcome/cheques/', 'refresh');
         }
       } else {
       //echo "Seleccione un Archivo EXCEL";
         redirect('/Welcome/cheques/', 'refresh');
      }
    }//guardar_excel


    //Agregar nuevo

    public function monthToString($month){
      switch ($month) {
        case 01:          $month ="ENERO";          break;
        case 1:          $month ="ENERO";          break;
        case 02:          $month ="FEBRERO";          break;
        case 2:          $month ="FEBRERO";          break;
        case 03:          $month ="MARZO";          break;
        case 3:          $month ="MARZO";          break;
        case 04:          $month ="ABRIL";          break;
        case 4:          $month ="ABRIL";          break;
        case 05:          $month ="MAYO";          break;
        case 5:          $month ="MAYO";          break;
        case 06:          $month ="JUNIO";          break;
        case 6:          $month ="JUNIO";          break;
        case 07:          $month ="JULIO";          break;
        case 7:          $month ="JULIO";          break;
        case 08:          $month ="AGOSTO";          break;
        case 8:          $month ="AGOSTO";          break;
        case 09:          $month ="SEPTIEMBRE";          break;
        case 9:          $month ="SEPTIEMBRE";          break;
        case 10:          $month ="OCTUBRE";          break;
        case 11:          $month ="NOVIEMBRE";          break;
        case 12:          $month ="DICIEMBRE";          break;
        default:          $month ="";          break;
      }
      return $month;
    }

    public function agregarNuevo(){
      $data['datos']=$this->input->post('dato');

      $this->load->view('head');
      $this->load->view('encabezado');
      $this->load->view('agregar',$data);
      $this->load->view('footer');
    }//agregarNuevo

    public function agregar_caja(){
      $data = array(
        'Fecha' => date('d/m/Y',strtotime($this->input->post('Fecha'))),
        'Concepto' => $this->input->post('Concepto'),
        'Entrada' => $this->input->post('Entrada'),
        'Salida' => $this->input->post('Salida'),
        'Saldo' => $this->input->post('Saldo'),
        'Pagina' => $this->getPagina($this->input->post('radio'))
     );
      $this->bases_model->insertTablaCajaChica($data);
      redirect('/Welcome/caja/', 'refresh');
    }

    public function agregar_fondofijo(){
      $year  = date('Y',strtotime($this->input->post('Fecha')));
      $tmonth  = date('m',strtotime($this->input->post('Fecha')));
      $month = $this->monthToString($tmonth);

      $temp=array(
        'month'=>$tmonth,
        'year'=>$year,
        'pag'=>$this->getPagina($this->input->post('radio'))
      );
      $saldo=$this->bases_model->getMinSaldoff1($temp);
      // var_dump($saldo);
      $total=$saldo-$this->input->post('Salida');
      // var_dump($total);

      $data = array(
        'Fecha' => date('d/m/Y',strtotime($this->input->post('Fecha'))),
        'Folio' => $this->input->post('Folio'),
        'Razon_social' => $this->input->post('Razon_social'),
        'RFC' => $this->input->post('RFC'),
        'Concepto' => $this->input->post('Concepto'),
        'Monto' => $this->input->post('Monto'),
        'IVA' => $this->input->post('IVA'),
        'Salida' => $this->input->post('Salida'),
        'Saldo' => $total,
        'Pagina' => $this->getPagina($this->input->post('radio')),
        'Banco' => $this->getBanco($this->input->post('radio1')),
        'Mes' => $month,
        'Anio' => $year
     );

      $this->bases_model->insertTablaFondofijo1($data);
      redirect('/Welcome/fondoFijo/', 'refresh');
    }

    public function agregar_importefondofijo(){
      $year  = date('Y',strtotime($this->input->post('Fecha')));
      $tmonth  = date('m',strtotime($this->input->post('Fecha')));
      $month = $this->monthToString($tmonth);
      $temp=array(
        'month'=>$tmonth,
        'year'=>$year,
        'pag'=>$this->getPagina($this->input->post('radio'))
      );
      $data = array(
        'Fecha' => date('d/m/Y',strtotime($this->input->post('Fecha'))),
        'Importe' => $this->input->post('Importe'),
        'Pagina' => $this->getPagina($this->input->post('radio')),
        'Banco' => $this->getBanco($this->input->post('radio1')),
        'Mes' => $month,
        'Anio' => $year
     );

      $this->bases_model->insertTablaFondofijo2($data);
      $this->bases_model->updateData($temp);
      redirect('/Welcome/impfondoFijo/', 'refresh');
    }

    public function agregar_banco(){
      $data = array(
        'Fecha' => date('d/m/Y',strtotime($this->input->post('Fecha'))),
        'Descripcion' => $this->input->post('Descripcion'),
        'Referencia' => $this->input->post('Referencia'),
        'Cargos' => $this->input->post('Cargo'),
        'Abonos' => $this->input->post('Abonos'),
        'Estatus_comprobacion' => $this->input->post('Estatus_comprobacion'),
        'Pagina' => $this->getPagina($this->input->post('radio')),
        'Banco' => $this->getBanco($this->input->post('radio1'))
     );
      $this->bases_model->insertTablaBanco($data);
      redirect('/Welcome/banco/', 'refresh');
    }

    public function agregar_facturas(){
      $year  = date('Y',strtotime($this->input->post('Fecha')));
      $month  = date('m',strtotime($this->input->post('Fecha')));
      $month = $this->monthToString($month);

      $data = array(
        'Folio' => $this->input->post('Folio'),
        'Concepto' => $this->input->post('Concepto'),
        'Fecha' => date('d/m/Y',strtotime($this->input->post('Fecha'))),
        'Importe' => $this->input->post('Importe'),
        'Retenciones' => $this->input->post('Retenciones'),
        'IVA' => $this->input->post('IVA'),
        'moneda' => $this->input->post('moneda'),
        'Total' => $this->input->post('Total'),
        'Razon_social' => $this->input->post('Razon_social'),
        'Estado' => $this->input->post('Estado'),
        'Pagina' => $this->getPagina($this->input->post('radio')),
        'Mes' => $month,
        'Anio' => $year
     );
      $this->bases_model->insertTablaFactura($data);
      redirect('/Welcome/facturas/', 'refresh');
    }

    public function agregar_nomina(){
      $data = array(
        'Folio' => $this->input->post('Folio'),
        'Periodo' => $this->input->post('Periodo'),
        'Concepto' => $this->input->post('Concepto'),
        'Fecha' => date('d/m/Y',strtotime($this->input->post('Fecha'))),
        'Importe' => $this->input->post('Importe'),
        'IMSS' => $this->input->post('IMSS'),
        'Subsidio' => $this->input->post('Subsidio'),
        'ISR' => $this->input->post('ISR'),
        'Empleado' => $this->input->post('Empleado'),
        'Pagina' => $this->getPagina($this->input->post('radio'))
     );
      $this->bases_model->insertTablaNomina($data);
      redirect('/Welcome/nomina/', 'refresh');
    }

    public function agregar_cpp(){
      $data = array(
        'Fecha' => date('d/m/Y',strtotime($this->input->post('Fecha'))),
        'Concepto' => $this->input->post('Concepto'),
        'Empresa' => $this->input->post('Empresa'),
        'Importe' => $this->input->post('Importe'),
        'Nota' => $this->input->post('Nota'),
        'Estado' => $this->input->post('Estado'),
        'Pagina' => $this->getPagina($this->input->post('radio'))
     );
      $this->bases_model->insertTablaCPP($data);
      redirect('/Welcome/cpp/', 'refresh');
    }

    public function agregar_cpc(){
      $data = array(
        'Concepto' => $this->input->post('Concepto'),
        'Empresa' => $this->input->post('Empresa'),
        'Cantidad' => $this->input->post('Cantidad'),
        'Fecha_limite' => date('d/m/Y',strtotime($this->input->post('Fecha_limite'))),
        'Nota' => $this->input->post('Nota'),
        'Estatus' => $this->input->post('Estatus'),
        'Pagina' => $this->getPagina($this->input->post('radio'))
     );
      $this->bases_model->insertTablaCPC($data);
      redirect('/Welcome/cpc/', 'refresh');
    }

    public function agregar_servicios(){
      $data = array(
        'Servicio' => $this->input->post('service'),
        'Tipo' => $this->getServicio($this->input->post('radio2')),
        'Folio' => $this->input->post('Folio'),
        'Fecha_emision' => date('d/m/Y',strtotime($this->input->post('Fecha_emision'))),
        'Fecha_pago' => date('d/m/Y',strtotime($this->input->post('Fecha_pago'))),
        'Forma_pago' => $this->input->post('Forma_pago'),
        'Periodo' => $this->input->post('Periodo'),
        'Importe' => $this->input->post('Importe'),
        'Pagina' => $this->getPagina($this->input->post('radio'))
     );
      $this->bases_model->insertTablaServicios($data);
      redirect('/Welcome/servicios/', 'refresh');
    }

    public function agregar_sat(){
      $data = array(
        'Mes' => $this->input->post('mes'),
        'Año' => $this->input->post('anio'),
        'Ntotal' => $this->input->post('Ntotal'),
        'Nfpre' => date('d/m/Y',strtotime($this->input->post('Nfpre'))),
        'Nfpag' => date('d/m/Y',strtotime($this->input->post('Nfpag'))),
        'C1total' => $this->input->post('C1total'),
        'C1fpre' => date('d/m/Y',strtotime($this->input->post('C1fpre'))),
        'C1fpag' => date('d/m/Y',strtotime($this->input->post('C1fpag'))),
        'C2total' => $this->input->post('C2total'),
        'C2fpre' => date('d/m/Y',strtotime($this->input->post('C2fpre'))),
        'C2fpag' => date('d/m/Y',strtotime($this->input->post('C2fpag'))),
        'C3total' => $this->input->post('C3total'),
        'C3fpre' => date('d/m/Y',strtotime($this->input->post('C3fpre'))),
        'C3fpag' => date('d/m/Y',strtotime($this->input->post('C3fpag'))),
        'C4total' => $this->input->post('C4total'),
        'C4fpre' => date('d/m/Y',strtotime($this->input->post('C4fpre'))),
        'C4fpag' => date('d/m/Y',strtotime($this->input->post('C4fpag'))),
        'Pagina' => $this->getPagina($this->input->post('radio'))
     );
      $this->bases_model->insertTablaSat($data);
      redirect('/Welcome/sat/', 'refresh');
    }

    public function agregar_requerimientos(){
      $data = array(
        'Fecha' => date('d/m/Y',strtotime($this->input->post('Fecha'))),
        'Entidad' => $this->input->post('inputentidad'),
        'Documento' => $this->input->post('inputtipodoc'),
        'Solicitud' => $this->input->post('inputtiposol'),
        'Folio' => $this->input->post('Folio'),
        'Comentarios' => $this->input->post('Comentarios'),
        'Pagina' => $this->getPagina($this->input->post('radio'))
     );
      $this->bases_model->insertTablaRequerimientos($data);
      redirect('/Welcome/requerimientos/', 'refresh');
    }

    public function agregar_controlinterno(){
      $data = array(
        'Fecha' => date('d/m/Y',strtotime($this->input->post('Fecha'))),
        'Cantidad' => $this->input->post('Cantidad'),
        'Empresa' => $this->getMutuante($this->input->post('Mutuante')),
        'Contratista' => $this->getMutuario($this->input->post('Mutuario')),
        'Documento' => $this->getdoc($this->input->post('doc')),
        'Pagina' => $this->getPagina($this->input->post('radio'))
     );
      $this->bases_model->insertTablaControlinterno($data);
      redirect('/Welcome/controlIntterno/', 'refresh');
    }

    public function agregar_cheques(){
      $data = array(
        'Fecha' => date('d/m/Y',strtotime($this->input->post('Fecha'))),
        'Cheque' => $this->input->post('Cheque'),
        'Cliente' => $this->input->post('Cliente'),
        'Subtotal' => $this->input->post('Subtotal'),
        'Total' => $this->input->post('Total'),
        'Banco' => $this->getBanco($this->input->post('radio1')),
        'Estado' => $this->getPagina($this->input->post('radio'))
     );
      $this->bases_model->insertTablaCheques($data);
      redirect('/Welcome/cheques/', 'refresh');
    }


}

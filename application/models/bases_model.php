<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class bases_model extends CI_Model {

  function __construct()
    {parent::__construct();}

  public function validaLogin($data){
   $cadena="select * from usuarios where login='".$data['login']."' and password='".$data['password']."'";
    $query = $this->db->query($cadena);
    if ($query->num_rows() > 0)
        {return $query;
    }else{
      return FALSE;
    }
  }

  public function insertTablaCajaChica($data){
    $temp0=implode("",explode(" ",$data['Fecha']));
    $temp1=explode("/",$temp0);
    if (count($temp1)<2){
      $temp1=explode("-",$temp0);
    }
    //echo("<script>console.log('cadena ".$temp1[0]." | ".$temp1[1]." | ".$temp1[2]."');</script>");
    if (count($temp1)>2){
      if (is_numeric($temp1[0]) && is_numeric($temp1[1]) && is_numeric($temp1[2])){
        $cadena="insert into cajachica(Fecha,Concepto,Entrada,Salida,Saldo,Pagina) values (STR_TO_DATE(REPLACE('".$data['Fecha']."','/','.') ,GET_FORMAT(date,'EUR')),'".$data['Concepto']."','".$data['Entrada']."','".$data['Salida']."','".$data['Saldo']."','".$data['Pagina']."')";
        $this->db->trans_begin();
        $this->db->query($cadena);
        if ($this->db->trans_status() === FALSE)
         {
            $this->db->trans_rollback();
         }
        else
         {
            $this->db->trans_commit();
         }
      }
    }
  }//insertTablaCajaChica

  public function insertTablaBanco($data){
    $temp0=implode("",explode(" ",$data['Fecha']));
    $temp1=explode("/",$temp0);
    if (count($temp1)<2){
      $temp1=explode("-",$temp0);
    }
    //echo("<script>console.log('cadena ".$temp1[0]." | ".$temp1[1]." | ".$temp1[2]."');</script>");
    if (count($temp1)>2){
      if (is_numeric($temp1[0]) && is_numeric($temp1[1]) && is_numeric($temp1[2])){
        $cadena= "insert into bancos(Fecha,Descripcion,Referencia,Cargos,Abonos,Estatus_comprobacion,Banco,Pagina) VALUES (STR_TO_DATE(REPLACE('".$data['Fecha']."','/','.') ,GET_FORMAT(date,'EUR')),'".$data['Descripcion']."','".$data['Referencia']."','".$data['Cargos']."','".$data['Abonos']."','".$data['Estatus_comprobacion']."','".$data['Banco']."','".$data['Pagina']."')";
        $this->db->trans_begin();
        $this->db->query($cadena);
        if ($this->db->trans_status() === FALSE)
         {
            $this->db->trans_rollback();
         }
        else
         {
            $this->db->trans_commit();
         }
      }
    }
  }//insertTablaBanco

  public function insertTablaFondofijo1($data){
    $cadena= "insert into fondofijo(Fecha,Folio,Razon_social,RFC,Concepto,Monto,IVA,Salida,Saldo,Banco,Pagina,Mes,Anio) VALUES (STR_TO_DATE(REPLACE('".$data['Fecha']."','/','.') ,GET_FORMAT(date,'EUR')),'".
    $data['Folio']."','".$data['Razon_social']."','".$data['RFC']."','".$data['Concepto']."','".$data['Monto']."','".$data['IVA']."','".$data['Salida']."','".$data['Saldo']."','".$data['Banco']."','".$data['Pagina']."','".$data['Mes']."','".$data['Anio']."')";
    $this->db->trans_begin();
    $this->db->query($cadena);
    if ($this->db->trans_status() === FALSE)
     {
        $this->db->trans_rollback();
     }
    else
     {
        $this->db->trans_commit();
     }
  }//insertTablaFondofijo1

  public function insertTablaFondofijo2($data){
    $cadena= "insert into importefondofijo(Fecha,Importe,Banco,Pagina,Mes,Anio) VALUES (STR_TO_DATE(REPLACE('".$data['Fecha']."','/','.') ,GET_FORMAT(date,'EUR')),'".
    $data['Importe']."','".$data['Banco']."','".$data['Pagina']."','".$data['Mes']."','".$data['Anio']."')";
    $this->db->trans_begin();
    $this->db->query($cadena);
    if ($this->db->trans_status() === FALSE)
     {
        $this->db->trans_rollback();
     }
    else
     {
        $this->db->trans_commit();
     }
  }//insertTablaFondofijo2

  public function insertTablaFactura($data){
    $cadena= "insert into facturas(Folio,Concepto,Fecha,Importe,Retenciones,IVA,Moneda,Total,Razon_social,Estado,Pagina,Mes,Anio) VALUES ('".$data['Folio']."','".$data['Concepto']."',"."STR_TO_DATE(REPLACE('".$data['Fecha']."','/','.') ,GET_FORMAT(date,'EUR')),'".$data['Importe']."','".$data['Retenciones']."','".$data['IVA']."','".$data['moneda']."','".$data['Total']."','".$data['Razon_social']."','".$data['Estado']."','".$data['Pagina']."','".$data['Mes']."','".$data['Anio']."')";
    $this->db->trans_begin();
    $this->db->query($cadena);
    if ($this->db->trans_status() === FALSE)
     {
        $this->db->trans_rollback();
     }
    else
     {
        $this->db->trans_commit();
     }
  }//insertTablaFactura

  public function insertTablaNomina($data){
    $cadena= "insert into nomina(Folio,Periodo,Fecha,Importe,IMSS,Subsidio,ISR,Empleado,Pagina) VALUES ('".$data['Folio']."','".$data['Periodo']."',"."STR_TO_DATE(REPLACE('".$data['Fecha']."','/','.') ,GET_FORMAT(date,'EUR')),'".$data['Importe']."','".$data['IMSS']."','".$data['Subsidio']."','".$data['ISR']."','".$data['Empleado']."','".$data['Pagina']."')";
    $this->db->trans_begin();
    $this->db->query($cadena);
    if ($this->db->trans_status() === FALSE)
     {
        $this->db->trans_rollback();
     }
    else
     {
        $this->db->trans_commit();
     }
  }//insertTablaNomina

  public function insertTablaCPP($data){
    $cadena= "insert into cpp(Fecha,Concepto,Empresa,Importe,Nota,Estado,Pagina) VALUES (STR_TO_DATE(REPLACE('".$data['Fecha']."','/','.') ,GET_FORMAT(date,'EUR')),'".$data['Concepto']."','".$data['Empresa']."','".$data['Importe']."','".$data['Nota']."','".$data['Estado']."','".$data['Pagina']."')";
    $this->db->trans_begin();
    $this->db->query($cadena);
    if ($this->db->trans_status() === FALSE)
     {
        $this->db->trans_rollback();
     }
    else
     {
        $this->db->trans_commit();
     }
  }//insertTablaCPP

  public function insertTablaCPC($data){
    $cadena= "insert into cpc(Concepto,Empresa,Cantidad,Fecha_limite,Nota,Estatus,Pagina) VALUES ('".$data['Concepto']."','".$data['Empresa']."','".$data['Cantidad']."',STR_TO_DATE(REPLACE('".$data['Fecha_limite']."','/','.') ,GET_FORMAT(date,'EUR')),'".$data['Nota']."','".$data['Estatus']."','".$data['Pagina']."')";
    $this->db->trans_begin();
    $this->db->query($cadena);
    if ($this->db->trans_status() === FALSE)
     {
        $this->db->trans_rollback();
     }
    else
     {
        $this->db->trans_commit();
     }
  }//insertTablaCPC

  public function insertTablaServicios($data){
    $temp="'Null'";
    if ($data['Fecha_pago']!=Null){
      $temp="STR_TO_DATE(REPLACE('".$data['Fecha_pago']."','/','.') ,GET_FORMAT(date,'EUR'))";
    }
    $cadena= "insert into servicios(Tipo,Servicio,Folio,Periodo,Fecha_emision,Fecha_pago,Forma_pago,Importe,Pagina) VALUES ('".$data['Tipo']."','".$data['Servicio']."','".$data['Folio']."','".$data['Periodo']."',".
    "STR_TO_DATE(REPLACE('".$data['Fecha_emision']."','/','.') ,GET_FORMAT(date,'EUR')),".
    $temp.",'".
    $data['Forma_pago']."','".$data['Importe']."','".$data['Pagina']."')";
    $this->db->trans_begin();
    $this->db->query($cadena);
    if ($this->db->trans_status() === FALSE)
     {
        $this->db->trans_rollback();
     }
    else
     {
        $this->db->trans_commit();
     }
  }//insertTablaServicios

  public function dateornull($fecha){
    $temp="'Null'";
    if ($fecha!=Null){
      $temp="STR_TO_DATE(REPLACE('".$fecha."','/','.') ,GET_FORMAT(date,'EUR'))";
    }
    return $temp;
  }

  public function insertTablaSat($data){
    $Nfpag=$this->dateornull($data['Nfpag']);
    $C1fpre=$this->dateornull($data['C1fpre']);
    $C1fpag=$this->dateornull($data['C1fpag']);
    $C2fpre=$this->dateornull($data['C2fpre']);
    $C2fpag=$this->dateornull($data['C2fpag']);
    $C3fpre=$this->dateornull($data['C3fpre']);
    $C3fpag=$this->dateornull($data['C3fpag']);
    $C4fpre=$this->dateornull($data['C4fpre']);
    $C4fpag=$this->dateornull($data['C4fpag']);
    $cadena= "insert into sat_nuevo(Mes,Año,Ntotal,Nfpre,Nfpag,C1total,C1fpre,C1fpag,C2total,C2fpre,C2fpag,C3total,C3fpre,C3fpag,C4total,C4fpre,C4fpag,Pagina) VALUES ('".$data['Mes']."','".$data['Año']."','".
    $data['Ntotal']."',"."STR_TO_DATE(REPLACE('".$data['Nfpre']."','/','.') ,GET_FORMAT(date,'EUR')),".$Nfpag.",'".
    $data['C1total']."',".$C1fpre.",".$C1fpag.",'".
    $data['C2total']."',".$C2fpre.",".$C2fpag.",'".
    $data['C3total']."',".$C3fpre.",".$C3fpag.",'".
    $data['C4total']."',".$C4fpre.",".$C4fpag.",'".$data['Pagina']."')";
    $this->db->trans_begin();
    $this->db->query($cadena);
    if ($this->db->trans_status() === FALSE)
     {
        $this->db->trans_rollback();
     }
    else
     {
        $this->db->trans_commit();
     }
  }//insertTablaSat

  public function insertTablaRequerimientos($data){
    $cadena= "insert into requerimientos(Fecha,Entidad,Documento,Solicitud,Folio,Comentarios,Pagina) VALUES (STR_TO_DATE(REPLACE('".$data['Fecha']."','/','.') ,GET_FORMAT(date,'EUR')),'".
    $data['Entidad']."','".$data['Documento']."','".$data['Solicitud']."','".$data['Folio']."','".$data['Comentarios']."','".$data['Pagina']."')";
    $this->db->trans_begin();
    $this->db->query($cadena);
    if ($this->db->trans_status() === FALSE)
     {
        $this->db->trans_rollback();
     }
    else
     {
        $this->db->trans_commit();
     }
  }//insertTablaRequerimientos

  public function insertTablaControlinterno($data){
    $cadena= "insert into control_interno(Fecha,Cantidad,Empresa,Contratista,Documento,Pagina) VALUES (STR_TO_DATE(REPLACE('".$data['Fecha']."','/','.') ,GET_FORMAT(date,'EUR')),'".
    $data['Cantidad']."','".$data['Empresa']."','".$data['Contratista']."','".$data['Documento']."','".$data['Pagina']."')";
    $this->db->trans_begin();
    $this->db->query($cadena);
    if ($this->db->trans_status() === FALSE)
     {
        $this->db->trans_rollback();
     }
    else
     {
        $this->db->trans_commit();
     }
  }//insertTablaControlinterno

  public function insertTablaCheques($data){
    $cadena= "insert into cheques(Fecha,Banco,Cheque,Cliente,Subtotal,Total,Estado) VALUES (STR_TO_DATE(REPLACE('".$data['Fecha']."','/','.') ,GET_FORMAT(date,'EUR')),'".
    $data['Banco']."','".$data['Cheque']."','".$data['Cliente']."','".$data['Subtotal']."','".$data['Total']."','".$data['Estado']."')";
    $this->db->trans_begin();
    $this->db->query($cadena);
    if ($this->db->trans_status() === FALSE)
     {
        $this->db->trans_rollback();
     }
    else
     {
        $this->db->trans_commit();
     }
  }//insertTablaControlinterno





  public function getSAT($data){
   $cadena="select Mes, Ntotal, Año from sat_nuevo where Año='".$data['year']."' and Pagina='".$data['pag']."'";
    $query = $this->db->query($cadena);
    if ($query->num_rows() > 0)
        {return $query;
    }else{
      return FALSE;
    }
  }//Fin->getSAT


  public function getSATyear(){
   $cadena="select distinct Año from sat_nuevo";
    $query = $this->db->query($cadena);
    if ($query->num_rows() > 0)
        {return $query;
    }else{
      return FALSE;
    }
  }//Fin->getSATyear

  public function getServicio($data){
    $cadena="select Servicio, Periodo, Importe, Year(Fecha_emision) as year, Month(Fecha_emision) as month, Day(Fecha_emision) as day  from servicios where YEAR(Fecha_emision)='".$data['year']."' and Tipo like '%".$data['servicio']."%' and Pagina='".$data['pag']."'";
     $query = $this->db->query($cadena);
     if ($query->num_rows() > 0)
         {return $query;
     }else{
       return FALSE;
     }


  }//findegetServicio

  public function getServicioyear(){
   $cadena="select distinct Year(Fecha_emision) as year from servicios";
    $query = $this->db->query($cadena);
    if ($query->num_rows() > 0)
        {return $query;
    }else{
      return FALSE;
    }
  }//Fin->getSATyear

  public function getSaldoImpFF($data){
    $cadena = "select Importe from importefondofijo where month(Fecha)=".$data['month']." and year(Fecha)=".$data['year']." and Pagina = '".$data['pag']."'";
    $query = $this->db->query($cadena);
    $suma=0;
    if ($query->num_rows() > 0){
     foreach ($query->result() as $row){
        $suma+=floatval($row->Importe);
     }
     // echo "suma".$suma;
     return $suma;
    }
    return $suma;
  }

  public function getMinSaldoff1($data){
    $cadena = "select Saldo from fondofijo where month(Fecha)=".$data['month']." and year(Fecha)=".$data['year']." and Pagina = '".$data['pag']."'";
    $query = $this->db->query($cadena);
    $min=999999999;
    if ($query->num_rows() > 0){
       foreach ($query->result() as $row){
          if ($min > floatval ($row->Saldo)){
            $min=floatval($row->Saldo);
          }
        // var_dump("|row=".$row->Saldo);
       }
       // var_dump("minimo".$min);
       return $min;
    }else{
      $suma=$this->getSaldoImpFF($data);
      return $suma;
    }
  }//Fin->getMinSaldoff1

  public function updateData($data){
    $saldoff=$this->getSaldoImpFF($data);
    $cadena = "select * from fondofijo where month(Fecha)=".$data['month']." and year(Fecha)=".$data['year']." and Pagina = '".$data['pag']."'";
    $query = $this->db->query($cadena);
    if ($query->num_rows() > 0){
     foreach ($query->result() as $row){
        $saldoff=$saldoff - $row->Salida;
        $actualiza="update fondofijo set Saldo = '".$saldoff."' where fondofijo.id_ff='".$row->id_ff."'";
        if ($this->db->query($actualiza) === TRUE) {
          echo "Record updated successfully";
        } else {
          echo "Error updating record: " . $this->db->error;
        }
     }
     return True;
    }
    return False;
  }


}
